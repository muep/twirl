#!/usr/bin/env python
#
# Copyright (c) 2014 Joonas Sarajärvi
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
#
# This is a shortcut script for setting up common build directories
# on a GNU/Linux build system, to ease recreation of these on
# various hosts where twirl is developed. Just an alternative for
# invoking cmake manually for each of the desired build
# configurations.

from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals

import os
import subprocess as sp

def cmake_buildtype(buildtype):
    types = {
        "debug" : "Debug",
        "release" : "Release"
        }
    return types[buildtype]

def gcc_config(buildtype):
    dir = "build-" + buildtype + "-gcc"
    btype = cmake_buildtype(buildtype)

    config = {
        "dir" : dir,
        "cc" : "gcc",
        "cflags" : "-Wall -Wextra -std=c11",
        "cxx" : "g++",
        "cxxflags" : "-Wall -Wextra -std=c++11",
        "btype" : btype
        }
    return config

def llvm_config(buildtype):
    dir = "build-" + buildtype + "-llvm"
    config = gcc_config(buildtype)
    config["dir"] = dir
    config["cc"] = "clang"
    config["cxx"] = "clang++"

    return config

configs = [
    gcc_config("debug"),
    gcc_config("release"),
    llvm_config("debug"),
    llvm_config("release")
    ]

def try_setup(config):
    dir = config["dir"]

    cmake_env = {
        "PATH" : os.environ["PATH"],
        "CC" : config["cc"],
        "CXX" : config["cxx"],
        "CFLAGS" : config["cflags"],
        "CXXFLAGS" : config["cxxflags"]
        }

    cmake_params = [
        "-DTWIRL_USE_QT5=ON",
        "-DTWIRL_USE_OPENCV=ON",
        "-DCMAKE_BUILD_TYPE={}".format(config["btype"]),
        ".."
        ]

    try:
        os.makedirs(dir)
    except FileExistsError:
        print("{} exists, not overwriting".format(dir))
        return

    sp.call(["cmake"] + cmake_params,
            env=cmake_env,
            cwd=dir)

def main():
    for config in configs:
        try:
            try_setup(config)
        except Exception:
            print("could not perform setup for config:")
            print(config)

if __name__ == "__main__":
     main()
