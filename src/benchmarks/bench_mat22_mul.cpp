/*
Copyright (c) 2014 Joonas Sarajärvi

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/
#include <iostream>
#include <sstream>
#include <string>

#include <twirl/math.hpp>
#include <twirl/matrix.hpp>

namespace {

std::string
to_text(twirl::matrix<2,2> const mx)
{
	std::stringstream o;

	o << "[" << mx[0][0] << ", " << mx[0][1] << "," << std::endl;
	o << " " << mx[1][0] << ", " << mx[1][1] << "]" << std::endl;

	return o.str();
}

} /* namespace */

int main()
{
	using twirl::math::cosr;
	using twirl::math::pi;
	using twirl::math::sinr;
	using twirl::matrix;
	using twirl::real_t;

	auto const iterations = 100000000;
	real_t const r_angle = pi / iterations;

	real_t const rot_vals[] = {
		cosr(r_angle), -sinr(r_angle),
		sinr(r_angle),  cosr(r_angle)
	};
	matrix<2,2> const rot(rot_vals);

	auto acc = matrix<2,2>::identity();
	std::cout << to_text(acc);

	for (auto i = 0; i < iterations; ++i) {
		acc = acc * rot;
	}

	std::cout << to_text(acc);

	return 0;
}
