/*
Copyright (c) 2014 Joonas Sarajärvi

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/
#include <algorithm>
#include <cstdint>
#include <iostream>
#include <vector>

#include <twirl/polynomy.hpp>

int main(int, char **)
{
	using twirl::real_t;
	twirl::polynomy p;

	std::uint64_t const iterations = 10000000UL;

	real_t const k13579[] = {
		330.133f,
		-58.6278f,
		-23.4998f,
		86.5818f,
		2.20281f
	};

	real_t const bottom = 0.0f;
	real_t const top = 1.0f;

	real_t const interval = (top - bottom) / iterations;

	real_t max = 0;

	for (std::uint64_t n = 0; n < iterations; ++n) {
		real_t const x = n * interval;
		real_t const y = p.value_13579(k13579, x);

		max = std::max(max, y);
	}

	std::cout << max << std::endl;

	return 0;
}
