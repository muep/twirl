/*
Copyright (c) 2014 Joonas Sarajärvi

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/
#include <twirl/kannala.hpp>
#include <twirl/polynomy.hpp>

namespace {

twirl::kannala::full_model
example_model(void)
{
	twirl::kannala::full_model mdl;

	mdl.symmetric_part.k[0] = 1.0f;
	mdl.symmetric_part.k[1] = 0.1f;
	mdl.symmetric_part.k[2] = 0.0f;
	mdl.symmetric_part.k[3] = 0.0f;
	mdl.symmetric_part.k[4] = 0.0f;
	mdl.symmetric_part.th_max = 1.0f;

	mdl.radial_distortion.d_th[0] = 0;
	mdl.radial_distortion.d_th[1] = 0;
	mdl.radial_distortion.d_th[2] = 0;
	mdl.radial_distortion.d_ph[0] = 0;
	mdl.radial_distortion.d_ph[1] = 0;
	mdl.radial_distortion.d_ph[2] = 0;
	mdl.radial_distortion.d_ph[3] = 0;

	mdl.tangential_distortion.d_th[0] = 0;
	mdl.tangential_distortion.d_th[1] = 0;
	mdl.tangential_distortion.d_th[2] = 0;
	mdl.tangential_distortion.d_ph[0] = 0;
	mdl.tangential_distortion.d_ph[1] = 0;
	mdl.tangential_distortion.d_ph[2] = 0;
	mdl.tangential_distortion.d_ph[3] = 0;

	mdl.affine_part.mu = 512;
	mdl.affine_part.mv = 512;
	mdl.affine_part.u0 = 512;
	mdl.affine_part.v0 = 384;

	return mdl;
}

void
run_one(const twirl::polynomy &p,
        const twirl::kannala::full_model &model,
        int x,
        int y)
{
	using twirl::kannala::backproject;
	using twirl::vec2;
	using twirl::ray_direction;

	vec2 const pos = {
		static_cast<twirl::real_t>(x),
		static_cast<twirl::real_t>(y)
	};

	ray_direction const rd = backproject(p, model, pos);
	(void) rd;
}

}

int main(int, char **)
{
	auto const model = example_model();
	twirl::polynomy const p;

	int const size_x = 1024;
	int const size_y = 768;

	for (int y = 0; y < size_y; ++y) {
		for (int x = 0; x < size_x; ++x) {
			run_one(p, model, x, y);
		}
	}
}
