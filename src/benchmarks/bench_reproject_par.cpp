/*
Copyright (c) 2014 Joonas Sarajärvi

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/
#include <cstdint>
#include <iostream>
#include <string>
#include <vector>

#include <twirl/kannala.hpp>
#include <twirl/reproject.hpp>

#include <twirr/parallel_reproject.hpp>

namespace {

twirl::kannala::full_model
model()
{
	twirl::kannala::full_model m;

	/* These are some values that were copy-pasted from
	 * output of Kannala's calibration toolbox. */
	m.symmetric_part.k[0] = 330.133f;
	m.symmetric_part.k[1] = -58.6278f;
	m.symmetric_part.k[2] = -23.4998f;
	m.symmetric_part.k[3] = 86.5818f;
	m.symmetric_part.k[4] = 2.20281f;
	m.symmetric_part.th_max = 1.0f;

	m.tangential_distortion.d_th[0] = -0.261025f;
	m.tangential_distortion.d_th[1] = -3.44041f;
	m.tangential_distortion.d_th[2] = 5.06379f;
	m.tangential_distortion.d_ph[0] = 4.42883f;
	m.tangential_distortion.d_ph[1] = 3.54313f;
	m.tangential_distortion.d_ph[2] = -0.445914f;
	m.tangential_distortion.d_ph[3] = -0.22657f;

	m.radial_distortion.d_th[0] = -0.244443f;
	m.radial_distortion.d_th[1] = -3.78317f;
	m.radial_distortion.d_th[2] = 6.70563f;
	m.radial_distortion.d_ph[0] = -4.19283f;
	m.radial_distortion.d_ph[1] = 5.4702f;
	m.radial_distortion.d_ph[2] = -0.339141f;
	m.radial_distortion.d_ph[3] = -0.176185f;

	m.affine_part.mu = 3.31464f;
	m.affine_part.mv = 3.30748f;
	m.affine_part.u0 = 635.589f;
	m.affine_part.v0 = 364.252f;

	return m;
}

std::vector<std::uint32_t>
src_img(int const width, int const height)
{
	std::vector<std::uint32_t> result(width * height, 0);

	/* This is quite naive */
	uint32_t const white = 0xffffff00;

	for (int row = 0; row < height; ++row) {
		result[width * row] = white;
		result[width * row + 1] = white;
		result[width * row + width - 2] = white;
		result[width * row + width - 1] = white;
	}

	for (int col = 0; col < width; ++col) {
		result[col] = white;
		result[width * 1 + col] = white;
		result[width * (height - 2) + col] = white;
		result[width * (height - 1) + col] = white;
	}

	return result;
}

void f(unsigned int const iterations)
{
	int const width = 1280;
	int const height = 720;

	auto const src = src_img(width, height);
	auto const mdl = model();

	for (unsigned int n = 0; n < iterations; ++n) {
		std::vector<std::uint32_t> dest(width * height);

		twirr::parallel_reproject::parallel(
			mdl,
			width,
			height,
			0,
			src.data(),
			dest.data()
		);
	}
}

} /* namespace */

int
main(int argc, const char **argv)
{
	if (argc == 1) {
		f(1);
		return 0;
	}

	if (argc == 2) {
		try {
			unsigned int const iterations = std::stol(argv[1]);
			f(iterations);
			return 0;
		} catch (const std::exception &) {
			/* Silently ignore, will fall back to usage. */
		}
	}

	std::cerr << "usage: " << argv[0] << " [number of iterations]"
	          << std::endl;
	return 1;
}
