/*
Copyright (c) 2014 Joonas Sarajärvi

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/
#include <iostream>
#include <string>

#include <twirl/math.hpp>

namespace {

void f(unsigned int const iteration_mul)
{
	using twirl::math::cosr;
	using twirl::math::sinr;
	using twirl::real_t;

	auto const iterations = 1000000 * iteration_mul;
	real_t const step = 0.0001f;

	real_t sum = 0;

	for (unsigned int i = 0; i < iterations; ++i) {
		real_t const a = step * i;

		auto const c = cosr(a);
		auto const s = sinr(a);

		sum += c;
		sum += s;
	}

	std::cout << sum << std::endl;
}

} /* namespace */

int
main(int argc, const char **argv)
{
	if (argc == 1) {
		f(1);
		return 0;
	}

	if (argc == 2) {
		try {
			unsigned int const iterations = std::stol(argv[1]);
			f(iterations);
			return 0;
		} catch (const std::exception &) {
			/* Silently ignore, will fall back to usage. */
		}
	}

	std::cerr << "usage: " << argv[0] << " [number of iterations]"
	          << std::endl;
	return 1;
}
