/*
Copyright (c) 2014 Joonas Sarajärvi

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/
#include "TModelParamWidget.hpp"

#include <QFormLayout>
#include <QLabel>
#include <QDoubleSpinBox>

#include "TFunctions.hpp"

/**
 * Function for setting up the common properties of the
 * spinboxes.
 *
 * Returns a raw pointer from new that is owned by whoever
 * calls this.
 */
static
QDoubleSpinBox *
spinBox()
{
	QDoubleSpinBox * const box = new QDoubleSpinBox;
	box->setDecimals(2);
	box->setSingleStep(0.01);
	box->setMinimum(-10000);
	box->setMaximum(10000);
	return box;
}

TModelParamWidget::TModelParamWidget(QWidget* const parent,
                                     Qt::WindowFlags const f)
	: QWidget(parent, f)
{
	QFormLayout * const layout = new QFormLayout;

	auto const model = TFunctions::simple_model();

	for (unsigned int n = 0; n < (sizeof(m_boxes)/sizeof(*m_boxes)); ++n) {
		/* Make the box, initialize its value */
		m_boxes[n] = spinBox();

		QString title = "-";

		if (n < 5) {
			m_boxes[n]->setValue(model.symmetric_part.k[n]);
			title = "k" + QString::number(n);
		} else if (n < 8) {
			int const m = n - 5;
			m_boxes[n]->setValue(model.radial_distortion.d_th[m]);
			title = "rth" + QString::number(m);
		} else if (n < 12) {
			int const m = n - 8;
			m_boxes[n]->setValue(model.radial_distortion.d_ph[m]);
			title = "rph" + QString::number(m);
		} else if (n < 15) {
			int const m = n - 12;
			m_boxes[n]->setValue(model.tangential_distortion.d_th[m]);
			title = "tth" + QString::number(m);
		} else if (n < 19) {
			int const m = n - 15;
			m_boxes[n]->setValue(model.tangential_distortion.d_ph[m]);
			title = "tph" + QString::number(m);
		} else if (n == 19) {
			m_boxes[n]->setValue(model.affine_part.mu);
			title = "mu";
		} else if (n == 20) {
			m_boxes[n]->setValue(model.affine_part.mv);
			title = "mv";
		} else if (n == 21) {
			m_boxes[n]->setValue(model.affine_part.u0);
			title = "u0";
		} else if (n == 22) {
			m_boxes[n]->setValue(model.affine_part.v0);
			title = "v0";
		}

		/* Stuff into the UI */
		layout->addRow(title, m_boxes[n]);
		connect(m_boxes[n], SIGNAL(valueChanged(double)),
		        this, SIGNAL(modelChanged()));
	}

	setLayout(layout);
}

TModelParamWidget::~TModelParamWidget()
{
	/* Children will get cleaned automatically */
}

twirl::kannala::full_model
TModelParamWidget::model() const
{
	twirl::kannala::full_model mdl;

	mdl.symmetric_part.k[0] = m_boxes[0]->value();
	mdl.symmetric_part.k[1] = m_boxes[1]->value();
	mdl.symmetric_part.k[2] = m_boxes[2]->value();
	mdl.symmetric_part.k[3] = m_boxes[3]->value();
	mdl.symmetric_part.k[4] = m_boxes[4]->value();

	mdl.radial_distortion.d_th[0] = m_boxes[5]->value();
	mdl.radial_distortion.d_th[1] = m_boxes[6]->value();
	mdl.radial_distortion.d_th[2] = m_boxes[7]->value();
	mdl.radial_distortion.d_ph[0] = m_boxes[8]->value();
	mdl.radial_distortion.d_ph[1] = m_boxes[9]->value();
	mdl.radial_distortion.d_ph[2] = m_boxes[10]->value();
	mdl.radial_distortion.d_ph[3] = m_boxes[11]->value();

	mdl.tangential_distortion.d_th[0] = m_boxes[12]->value();
	mdl.tangential_distortion.d_th[1] = m_boxes[13]->value();
	mdl.tangential_distortion.d_th[2] = m_boxes[14]->value();
	mdl.tangential_distortion.d_ph[0] = m_boxes[15]->value();
	mdl.tangential_distortion.d_ph[1] = m_boxes[16]->value();
	mdl.tangential_distortion.d_ph[2] = m_boxes[17]->value();
	mdl.tangential_distortion.d_ph[3] = m_boxes[18]->value();

	/* Something like a 1024 x 1024 pixel sensor */
	mdl.affine_part.mu = m_boxes[19]->value();
	mdl.affine_part.mv = m_boxes[20]->value();
	mdl.affine_part.u0 = m_boxes[21]->value();
	mdl.affine_part.v0 = m_boxes[22]->value();

	return mdl;
}

void
TModelParamWidget::setModel(const twirl::kannala::full_model& model)
{
	int boxid = 0;
	for (auto value: model.symmetric_part.k) {
		m_boxes[boxid]->setValue(value);
		++boxid;
	}

	for (auto value: model.radial_distortion.d_th) {
		m_boxes[boxid]->setValue(value);
		++boxid;
	}

	for (auto value: model.radial_distortion.d_ph) {
		m_boxes[boxid]->setValue(value);
		++boxid;
	}

	for (auto value: model.tangential_distortion.d_th) {
		m_boxes[boxid]->setValue(value);
		++boxid;
	}

	for (auto value: model.tangential_distortion.d_ph) {
		m_boxes[boxid]->setValue(value);
		++boxid;
	}

	m_boxes[19]->setValue(model.affine_part.mu);
	m_boxes[20]->setValue(model.affine_part.mv);
	m_boxes[21]->setValue(model.affine_part.u0);
	m_boxes[22]->setValue(model.affine_part.v0);

	emit modelChanged();
}
