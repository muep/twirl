/*
Copyright (c) 2014 Joonas Sarajärvi

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/
#ifndef TWIRL_IMAGE_CORRECTION_HPP
#define TWIRL_IMAGE_CORRECTION_HPP

#include <QImage>

namespace twirl {
namespace kannala {
struct full_model;
}
}

/**
 * Implementations of the pure computation parts
 * of correction of distorted images.
 *
 * NOTE: While all the significant algorithms are supposed
 * to live as pure standard C++ code under the twirl
 * namespace, this is currently here because of the easy
 * availability of the QImage type in Qt. It may turn out
 * that some or all of this code is desired in the core
 * twirl parts. If that happens, the relevant things should
 * be implemented sans Qt in the twirl core.
 */
namespace TImageCorrection {

QImage
correctedImage(const QImage &img,
               const twirl::kannala::full_model &mdl);

} /* namespace TImageCorrection */

#endif
