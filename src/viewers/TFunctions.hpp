/*
Copyright (c) 2014 Joonas Sarajärvi

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/
#ifndef TWIRL_VIEWER_FUNCTIONS_HPP
#define TWIRL_VIEWER_FUNCTIONS_HPP

#include <QTransform>

#include <twirl.hpp>

#include <twirl/array.hpp>
#include <twirl/kannala.hpp>

namespace TFunctions {

template<int N>
twirl::array<twirl::ray_direction,4 * N*N>
raydirGrid(twirl::real_t const size,
           twirl::real_t const depth)
{
	using twirl::kannala::raydir_of_v3;
	using twirl::ray_direction;
	using twirl::vec3;

	twirl::array<ray_direction, 4 * N * N> result;

	auto const max_x = size;
	auto const max_y = size;
	auto const min_x = -max_x;
	auto const min_y = -max_y;
	auto const interval = (2 * size) / (2 * N - 1);

	int const top = 4 * N * N;
	int const pitch = 2 * N;

	for (int n = 0; n < top; ++n) {
		int const row = n / pitch;
		int const col = n % pitch;

		auto const x = min_x + interval * col;
		auto const y = min_y + interval * row;
		auto const z = -depth;
		vec3 v = { x, y, z };
		auto const dir = raydir_of_v3(v);
		result[n] = dir;
	}

	return result;
}

twirl::kannala::full_model
example_model();

twirl::kannala::full_model
simple_model();

} /* namespace TFunctions */

#endif
