/*
Copyright (c) 2014 Joonas Sarajärvi

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/
#include "TImageCorrectionView.hpp"

#include <QtConcurrent/QtConcurrent>
#include <QFuture>
#include <QFutureWatcher>
#include <QImage>
#include <QPainter>

#include "twirl/kannala.hpp"

#include "TImageCorrection.hpp"

static
QRect
scaledRect(QRect const src,
           QRect const target)
{
	auto const srcWidth  = static_cast<qreal>(src.width());
	auto const srcHeight = static_cast<qreal>(src.height());
	auto const tgtWidth  = static_cast<qreal>(target.width());
	auto const tgtHeight = static_cast<qreal>(target.height());

	if (srcWidth / srcHeight >= tgtWidth / tgtHeight) {
		/* Source image is of wider aspect ratio */
		auto const scalingFactor = tgtWidth / srcWidth;
		auto const scaledHeight = scalingFactor * tgtHeight;

		/* We likely have some extra vertical space */
		auto const surplus = (tgtHeight - scaledHeight) / 2;

		return QRect(0, surplus, tgtWidth, scaledHeight);
	} else {
		auto const scalingFactor = tgtHeight / srcHeight;
		auto const scaledWidth = scalingFactor * tgtWidth;

		auto const surplus = (tgtWidth - scaledWidth) / 2;
		return QRect(surplus, 0, scaledWidth, tgtHeight);
	}
}

class TImageCorrectionView::Private {
public:
	QImage original;
	QImage corrected;
	QFuture<QImage> futureCorrectedImage;
	QFutureWatcher<QImage> futureImageWatcher;
	twirl::kannala::full_model model;
};

TImageCorrectionView::TImageCorrectionView(QWidget* parent,
                                           Qt::WindowFlags f)
	: QWidget(parent, f),
	  d(new Private)
{
	auto theView = this;

	connect(&(d->futureImageWatcher), &QFutureWatcher<QImage>::finished,
	        [=]() {
		d->corrected = d->futureCorrectedImage.result();
		theView->update();
	});
}

TImageCorrectionView::~TImageCorrectionView()
{
	delete d;
}

QImage
TImageCorrectionView::currentCorrectedImage() const
{
	return d->corrected;
}

void TImageCorrectionView::paintEvent(QPaintEvent* )
{
	auto const r = scaledRect(d->corrected.rect(), this->rect());
	QPainter paintr(this);

	paintr.drawImage(r, d->corrected);
	if (d->futureImageWatcher.isRunning()) {
		paintr.drawText(10, 30, "Refreshing...");
	}
}

void TImageCorrectionView::setImage(QImage img)
{
	d->original = img;
	d->corrected = QImage();

	auto const mdlCopy = d->model;
	d->futureCorrectedImage = QtConcurrent::run([=]() {
		return TImageCorrection::correctedImage(img, mdlCopy);
	});
	d->futureImageWatcher.setFuture(d->futureCorrectedImage);
	update();
}

void
TImageCorrectionView::setModel(const twirl::kannala::full_model& mdl)
{
	d->model = mdl;

	auto const mdlCopy = d->model;
	auto const origCopy = d->original;
	d->futureCorrectedImage = QtConcurrent::run([=]() {
		return TImageCorrection::correctedImage(origCopy, mdlCopy);
	});

	d->futureImageWatcher.setFuture(d->futureCorrectedImage);
	update();
}
