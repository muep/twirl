/*
Copyright (c) 2014 Joonas Sarajärvi

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/
#include "TModelPlotView.hpp"

#include <algorithm>
#include <vector>

#include <QPaintDevice>
#include <QPainter>

#include <twirl/kannala.hpp>
#include <twirl/polynomy.hpp>

#include "TFunctions.hpp"

static
std::vector<twirl::real_t>
plot_points(const twirl::kannala::full_model &model,
            size_t const n_pts)
{
	using twirl::ray_direction;
	using twirl::real_t;
	using twirl::kannala::project_symmetric;

	std::vector<real_t> pts;
	pts.reserve(n_pts);

	real_t const phi = 0;

	twirl::polynomy const p;

	for (size_t col = 0; col < n_pts; ++col) {
		real_t const theta = col * 1.0f / n_pts;

		twirl::kannala::raydir_plus const rd(theta, phi);

		auto &sp = model.symmetric_part;
		auto const pos = project_symmetric(p, sp, rd);

		pts.push_back(pos.x);
	}

	return pts;
}

static
std::vector<QPointF>
pointf_vect(std::vector<twirl::real_t> const pts,
            twirl::real_t min,
            twirl::real_t max,
            qreal min_y,
            qreal max_y)
{
	std::vector<QPointF> out;

	if (pts.empty()) {
		return out;
	}

	out.reserve(pts.size());

	auto const pts_size = pts.size();

	if (max == min) {
		for (size_t n = 0; n < pts_size; ++n) {
			out.push_back(QPointF(n, 0));
		}
		return out;
	}

	auto const y_scale_factor = (max_y - min_y) / (max - min);

	for (size_t n = 0; n < pts_size; ++n) {
		auto y = pts[n];
		auto const rel_y = y - min;
		auto const scaled_y = rel_y * y_scale_factor;

		/* Here y gets inverted because our QPaintDevice
		 * seems to have y growing downwards. */
		out.push_back(QPointF(n, max_y - scaled_y));
	}

	return out;
}

static
void
drawPlot(const twirl::kannala::full_model &model,
         QPaintDevice * const device)
{
	auto const points = plot_points(model, device->width());

	auto const max =
	*(std::max_element(points.begin(), points.end()));
	auto const min =
	*(std::min_element(points.begin(), points.end()));

	auto const pointfs =
	pointf_vect(points, min, max, 0, device->height());

	QPainter p(device);
	p.setPen(Qt::black);

	auto const size = static_cast<int>(points.size());
	p.drawPolyline(&(pointfs[0]), size);

	auto const info = QString("%1 < r < %2").arg(min).arg(max);
	p.drawText(0, 20, info);
}

class TModelPlotView::Private {
public:
	Private()
		: model(TFunctions::example_model())
	{}
	twirl::kannala::full_model model;
};

TModelPlotView::TModelPlotView(QWidget* const parent)
	: QWidget(parent),
	  d(new Private)
{
}

TModelPlotView::~TModelPlotView()
{
	delete d;
}

void
TModelPlotView::paintEvent(QPaintEvent* const)
{
	drawPlot(d->model, this);
}

void
TModelPlotView::setModel(const twirl::kannala::full_model& mdl)
{
	d->model = mdl;
	update();
}
