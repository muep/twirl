/*
Copyright (c) 2014 Joonas Sarajärvi

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/
#include "TImageCorrection.hpp"

#include <QSize>

#include <twirl/kannala.hpp>
#include <twirl/math.hpp>
#include <twirl/pinhole.hpp>
#include <twirl/reproject.hpp>

#include <cstdint>
#include <cstring>
#include <vector>

namespace TImageCorrection {
namespace detail {
namespace {

std::vector<std::uint32_t>
dataOfQImage(QImage img)
{
	auto const supportedFormat = QImage::Format::Format_RGB32;
	if (img.format() != supportedFormat) {
		img = img.convertToFormat(supportedFormat);
	}

	auto const row_count = img.height();
	auto const col_count = img.width();

	auto const element_count =
	static_cast<size_t>(row_count * col_count);

	std::vector<std::uint32_t> result(element_count);
	auto const data = result.data();

	for (int row = 0; row < row_count; ++row) {
		std::memcpy(
			data + (row * col_count),
			img.scanLine(row),
			4 * col_count
		);
	}

	return result;
}

QImage
imageOfData(std::vector<std::uint32_t> const data,
            QSize const imgSize)
{
	auto const supportedFormat = QImage::Format::Format_RGB32;
	auto const height = imgSize.height();
	auto const width = imgSize.width();

	QImage img(width, height, supportedFormat);

	for (int row = 0; row < height; ++row) {
		std::memcpy(
			img.scanLine(row),
			data.data() + (row * width),
			4 * width
		);
	}

	return img;
}

} /* namespace */
} /* namespace detail */


QImage
correctedImage(const QImage &src,
               const twirl::kannala::full_model &km)
{
	auto const height = src.height();
	auto const width = src.width();

	std::vector<std::uint32_t> const data_in =
	detail::dataOfQImage(src);

	std::vector<twirl::reproject::lut_entry> lut(data_in.size());

	twirl::reproject::build_lut(
		km,
		width,
		height,
		lut.data()
	);

	std::vector<std::uint32_t> data_out(data_in.size());

	twirl::reproject::correct_with_lut(
		lut.data(),
		0,
		data_in.size(),
		qRgb(0, 0, 0),
		data_in.data(),
		data_out.data()
	);

	return detail::imageOfData(data_out, src.size());
}

} /* namespace TImageCorrection */
