/*
Copyright (c) 2014 Joonas Sarajärvi

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/
#include "TImageCorrectionUi.hpp"

#include <QFileDialog>
#include <QHBoxLayout>
#include <QPushButton>
#include <QVBoxLayout>

#include "TImageCorrectionView.hpp"

static
void
loadImage(QString const fname, TImageCorrectionView * const view)
{
	if (fname.length() == 0) {
		return;
	}

	QImage const img = QImage(fname);
	if (img.isNull()) {
		return;
	}

	view->setImage(img);
}

class TImageCorrectionUi::Private {
public:
	TImageCorrectionView *correctionView;
};

TImageCorrectionUi::TImageCorrectionUi(QWidget* parent, Qt::WindowFlags f)
	: QWidget(parent, f),
	  d(new Private)
{
	d->correctionView = new TImageCorrectionView;

	QPushButton * const loadButton =
	new QPushButton("Load image from file");

	connect(loadButton, &QPushButton::clicked,
	        [&]() {
		QString const title = "Select image file";

		auto const fname = QFileDialog::getOpenFileName(this, title);
		loadImage(fname, d->correctionView);
	});

	QPushButton * const saveButton =
	new QPushButton("Save current image");

	connect(saveButton, &QPushButton::clicked,
	        [&]() {
		QString const title = "Save as...";

		auto const img = d->correctionView->currentCorrectedImage();

		if (img.isNull()) {
			return;
		}

		auto const fname = QFileDialog::getSaveFileName(this, title);

		img.save(fname);
	});

	QHBoxLayout * const bottomBarLayout = new QHBoxLayout;
	bottomBarLayout->addWidget(loadButton);
	bottomBarLayout->addWidget(saveButton);

	QVBoxLayout * const mainLayout = new QVBoxLayout;
	mainLayout->addWidget(d->correctionView);
	mainLayout->addLayout(bottomBarLayout);

	setLayout(mainLayout);
}

TImageCorrectionUi::~TImageCorrectionUi()
{
	delete d;
}

void
TImageCorrectionUi::setModel(const twirl::kannala::full_model& mdl)
{
	d->correctionView->setModel(mdl);
}
