/*
Copyright (c) 2014 Joonas Sarajärvi

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/
#include <QApplication>
#include <QFileDialog>
#include <QHBoxLayout>
#include <QLabel>
#include <QMainWindow>
#include <QMessageBox>
#include <QPushButton>
#include <QTabWidget>
#include <QVBoxLayout>
#include <QWidget>

#include "TDistortionView.hpp"
#include "TImageCorrectionUi.hpp"
#include "TModelParamWidget.hpp"
#include "TModelPlotView.hpp"

int main(int argc, char **argv)
{
	QApplication app(argc, argv);

	TDistortionView * const distortionView = new TDistortionView;
	TModelParamWidget * const paramWidget = new TModelParamWidget;

	distortionView->setModel(paramWidget->model());

	TModelPlotView * const plotView = new TModelPlotView;
	plotView->setModel(paramWidget->model());

	TImageCorrectionUi * const cui = new TImageCorrectionUi;
	cui->setModel(paramWidget->model());

	QObject::connect(paramWidget, &TModelParamWidget::modelChanged,
	                 [=](){
		distortionView->setModel(paramWidget->model());
		plotView->setModel(paramWidget->model());
		cui->setModel(paramWidget->model());
	});

	QLabel * const paramLabel = new QLabel("Model parameters:");
	QPushButton * const loadButton = new QPushButton("Load from file");

	QTabWidget * const tabWidget = new QTabWidget;
	tabWidget->addTab(distortionView, "Distortion");
	tabWidget->addTab(plotView, "Plot");
	tabWidget->addTab(cui, "Correction");

	QVBoxLayout * const sideLayout = new QVBoxLayout;
	sideLayout->addWidget(paramLabel);
	sideLayout->addWidget(paramWidget);
	sideLayout->addWidget(loadButton);

	QHBoxLayout * const layout = new QHBoxLayout;
	layout->addLayout(sideLayout);
	layout->addWidget(tabWidget);

	QWidget * const centralWidget = new QWidget;
	centralWidget->setLayout(layout);

	QMainWindow window;
	window.setCentralWidget(centralWidget);

	QObject::connect(loadButton, &QPushButton::clicked,
	                 [&](){
		QString const caption = "Select model file";
		auto const fname =
			QFileDialog::getOpenFileName(&window, caption);

		if (fname.isEmpty()) {
			return;
		}

		QFile f(fname);

		if (!f.open(QIODevice::ReadOnly)) {
			auto const title = "Failed to open file";
			auto const msg =
			fname + " could not be opened for reading";

			QMessageBox::warning(&window, title, msg);
			return;
		}

		QByteArray const bytes = f.readAll();
		f.close();

		if (bytes.size() != 23 * 4) {
			auto const title = "Unexpected data";
			auto const n = QString::number(23 * 4);
			auto const msg =
			"Length of " + fname + " was not " + n + "bytes";

			QMessageBox::warning(&window, title, fname + msg);
			return;
		}

		auto raw_bytes =
		reinterpret_cast<const uint8_t*>(bytes.data());

		auto model =
		twirl::kannala::model_from_bytes(raw_bytes);

		paramWidget->setModel(model);
	});


	window.show();

	return app.exec();
}
