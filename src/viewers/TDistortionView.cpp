/*
Copyright (c) 2014 Joonas Sarajärvi

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/
#include "TDistortionView.hpp"

#include <QColor>
#include <QImage>
#include <QLabel>
#include <QPaintDevice>
#include <QPainter>
#include <QPen>
#include <QPixmap>
#include <QPointF>
#include <QTransform>

#include <twirl/kannala.hpp>
#include <twirl/math.hpp>
#include <twirl/polynomy.hpp>
#include <twirl/vec2_ops.hpp>

#include "TFunctions.hpp"

/**
 * Paint a colored cross to some location.
 */
static
void
paintCross(QPainter &ptr, QPoint const pt, QColor const color)
{
	auto const x = pt.x();
	auto const y = pt.y();

	ptr.save();

	ptr.setPen(color);

	ptr.drawLine(
		x - 2, y - 2,
		x + 2, y + 2
	);
	ptr.drawLine(
		x + 2, y - 2,
		x - 2, y + 2
	);

	ptr.restore();
}

static
twirl::real_t
max_r(const twirl::kannala::full_model &model)
{
	twirl::real_t result = 1;

	auto const th_max = 1;
	int const n_points = 12;
	twirl::real_t const angle_unit = 2 * twirl::math::pi / n_points;

	twirl::polynomy const p;

	for (int pos = 0; pos < n_points; ++pos) {
		twirl::real_t const phi = pos * angle_unit;
		twirl::ray_direction const rd = { th_max, phi };

		auto const symd =
		twirl::kannala::project_symmetric(
			p,
			model.symmetric_part,
			static_cast<twirl::kannala::raydir_plus>(rd)
		);

		auto const err =
		twirl::kannala::distortion(
			p,
			model.radial_distortion,
			model.tangential_distortion,
			static_cast<twirl::kannala::raydir_plus2>(rd)
		);

		auto const projd = symd + err;

		auto const r =
		twirl::math::sqrtr(projd.x * projd.x + projd.y * projd.y);

		if (r > result) {
			result = r;
		}
	}
	return result;
}

static
void
createPrettyImage(QPaintDevice * const target,
                  const twirl::kannala::full_model &model)
{
	int const height = target->height();
	int const width = target->width();

	auto const needed_space = max_r(model);
	auto const available_space = qMin(height / 2, width / 2);

	/* Number of pixels for representing a "one" */
	auto const unit = available_space / needed_space;

	/* A big array of directions of incoming light rays. */
	auto const srcPoints = TFunctions::raydirGrid<16>(10, 10);

	QColor const backgroundColor(0, 0, 0);
	QColor const lineColor(100, 100, 100);
	QColor const origPtColor(255, 0, 0);
	QColor const distortionColor(100, 0, 0);
	QColor const undistortionColor(0, 100, 0);
	QColor const fixedColor(0, 180, 60);

	QPainter paintr;
	paintr.begin(target);
	/* Let's get a coordinate system where origin is
	 * in the center of the image */
	paintr.translate(width / 2, height / 2);

	/* The "axis" lines */
	paintr.setPen(lineColor);
	paintr.drawLine(0, height / 2, 0, -height / 2);
	paintr.drawLine(width / 2, 0, -width / 2, 0);

	auto &sym = model.symmetric_part;
	auto &rad = model.radial_distortion;
	auto &tan = model.tangential_distortion;

	twirl::polynomy const p;

	for (auto pt : srcPoints) {
		using twirl::kannala::project_symmetric;
		using twirl::kannala::distortion;
		using twirl::kannala::undistortion;
		using twirl::kannala::backproject_symmetric;

		auto const plain = project_symmetric(
			p,
			sym,
			static_cast<twirl::kannala::raydir_plus>(pt)
		);

		auto const dif = distortion(
			p,
			rad,
			tan,
			static_cast<twirl::kannala::raydir_plus2>(pt)
		);

		/* Distorted value, using the full forward model */
		auto const dd = plain + dif;

		auto const dif2 = undistortion(p, model, dd);

		auto const ud = dd - dif2;

		/* Crosses to places where undistorted lens would
		 * put the rays */
		paintCross(
			paintr,
			QPoint(unit * plain.x, unit * plain.y),
			origPtColor
		);

		paintr.setPen(distortionColor);
		/* Line from original point to the distorted one */
		paintr.drawLine(
			unit * plain.x, unit * plain.y,
			unit * dd.x, unit * dd.y
		);

		paintr.setPen(undistortionColor);
		/* Then from distorted to undistorted */
		paintr.drawLine(
			unit * ud.x, unit * ud.y,
			unit * dd.x, unit * dd.y
		);

		/* And a cross again */
		paintCross(
			paintr,
			QPoint(unit * ud.x, unit * ud.y),
			fixedColor
		);
	}

	paintr.end();
}

class TDistortionView::Private {
public:
	Private()
	  : model(TFunctions::simple_model())
	{}
	twirl::kannala::full_model model;
};

TDistortionView::TDistortionView(QWidget* const parent,
                                 Qt::WindowFlags const f)
	: QWidget(parent,f),
	  d(new Private)
{
	setMinimumSize(200, 200);
	setModel(TFunctions::example_model());
}

TDistortionView::~TDistortionView()
{
}

void
TDistortionView::paintEvent(QPaintEvent* )
{
	createPrettyImage(this, d->model);
}

QSize
TDistortionView::sizeHint() const
{
    return QSize(400, 400);
}

void TDistortionView::setModel(const twirl::kannala::full_model& mdl)
{
	d->model = mdl;
	update();
}
