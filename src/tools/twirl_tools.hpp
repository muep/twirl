/*
Copyright (c) 2014 Joonas Sarajärvi

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/
#ifndef TWIRL_TOOLS_HPP
#define TWIRL_TOOLS_HPP

#include <cstdint>
#include <exception>
#include <string>
#include <vector>

#include <twirl/kannala.hpp>
#include <twirl/reproject.hpp>

#include <twirr/compiler_workarounds.hpp>

namespace twirl_tools {

struct model_load_failure : public std::exception {
	model_load_failure(std::string reason);
	~model_load_failure() noexcept;

	std::string reason;

	const char* what() const noexcept;
};

twirl::kannala::full_model
model_from_file_at(std::string const path);

std::string
text_of_model(const twirl::kannala::full_model &m);

std::vector<std::uint8_t>
whole_file_from(std::string path);

/**
 * A wrapper for LUT-building functions in
 * twirl::reproject. This will provide a multi-threaded
 * version if twirl is built with TWIRL_USE_THREADS=ON
 */
void
build_lut(const twirl::kannala::full_model &mdl,
          uint16_t width,
          uint16_t height,
          twirl::reproject::lut_entry *target);

} /* namespace twirl_tools */

#endif
