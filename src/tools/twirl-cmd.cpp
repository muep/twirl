/*
Copyright (c) 2014 Joonas Sarajärvi

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/
/*
 * twirl-cmd.cpp
 *
 * A command line interface to twirl.
 */

#include <cstdint>
#include <cstdio>
#include <exception>
#include <fstream>
#include <iostream>
#include <sstream>
#include <string>
#include <vector>

#include <twirl/kannala.hpp>
#include <twirl/math.hpp>
#include <twirl/polynomy.hpp>
#include <twirl/reproject.hpp>

#include <twirr/accuracy.hpp>
#include <twirr/cmdline.hpp>
#include <twirr/parallel_reproject.hpp>

#include "twirl_config.hpp"
#include "twirl_tools.hpp"
#include "png.hpp"

namespace {

void
show_model(twirr::cmdline::prefix_t const prefix,
           twirr::cmdline::args_t const args)
{
	using twirl_tools::model_from_file_at;
	using twirl_tools::text_of_model;

	if (args.size() != 1) {
		std::cerr << "usage: " << prefix << " <model-file>"
		          << std::endl;
		return;
	}

	auto const path = args[0];

	auto const model = model_from_file_at(path);

	std::cout << text_of_model(model);
}

void
estimate_max_theta(twirr::cmdline::prefix_t const prefix,
                   twirr::cmdline::args_t const args)
{
	using twirl_tools::model_from_file_at;

	if (args.size() != 3) {
		std::cerr << "Invalid number of parameters to "
		          << prefix
		          << std::endl
		          << "usage: " << prefix
		          << " <model> <width> <height>"
		          << std::endl;
		return;
	}

	twirl::isize size;
	twirl::polynomy const p;

	try {
		size.w = std::stoi(args[1]);
	} catch (const std::exception &) {
		std::cerr << "Could not interpret "
		          << args[1]
		          << " as width"
		          << std::endl;
		return;
	}

	try {
		size.h = std::stoi(args[2]);
	} catch (const std::exception &) {
		std::cerr << "Could not interpret "
		          << args[2]
		          << " as height"
		          << std::endl;
		return;
	}

	auto const model = model_from_file_at(args[0]);

	auto const max_theta =
	twirl::kannala::estimated_max_theta(p, model, size);

	std::cout << max_theta * 180 / twirl::math::pi << std::endl;
}

#ifdef TWIRL_USE_LIBPNG

png::image
reproject(const twirl::reproject::lut_entry * const lut,
          const png::image &src)
{
	static uint32_t const black = 0;

	auto const width = src.width;
	auto const height = src.height;

	auto const pixel_count = height * width;

	/* Be careful to fulfill this assumption. */
	auto const lut_size = pixel_count;

	std::vector<std::uint32_t> tmp_data(pixel_count);

#ifdef TWIRL_USE_THREADS
	twirr::parallel_reproject::with_lut_parallel(
		lut,
		lut_size,
		black,
		src.data.data(),
		tmp_data.data()
	);
#else
	twirl::reproject::correct_with_lut(
		lut,
		0,
		lut_size,
		black,
		src.data.data(),
		tmp_data.data()
	);
#endif

	return png::image(src.width, src.height, tmp_data.data());
}

void
reproject_image(twirr::cmdline::prefix_t const prefix,
                twirr::cmdline::args_t const args)
{
	using twirl::reproject::lut_entry;
	using twirl_tools::model_from_file_at;

	static char const usage[] =
		"<model> <file in> <file out>";

	if (args.size() != 3) {
		std::cerr << "Invalid number of parameters to "
		          << prefix << std::endl
		          << "usage: " << prefix << " "
		          << usage << std::endl;
		return;
	}

	auto const model = model_from_file_at(args[0]);

	auto const image_in_path = args[1];
	auto const image_in = png::load_png(image_in_path.c_str());
	if (image_in.height == 0 || image_in.width == 0) {
		std::cerr << "Failed to read an image from "
		          << image_in_path << std::endl;
		return;
	}

	std::vector<lut_entry> lut(image_in.width * image_in.height);

#ifdef TWIRL_USE_THREADS
	twirr::parallel_reproject::build_lut_parallel(
		model,
		image_in.width,
		image_in.height,
		lut.data()
	);

#else
	twirl_tools::build_lut(
		model,
		image_in.width,
		image_in.height,
		lut.data()
	);

#endif
	auto const image_out = reproject(lut.data(), image_in);

	png::save_png(args[2].c_str(), image_out);

	return;
}

enum class image_channel {
	red,
	green,
	blue,
	alpha
};

struct channel_name_error : public std::exception {
	static
	std::string
	msg_from_name(std::string name)
	{
		return "Invalid channel name: " + name;
	}

	channel_name_error(const std::string &name)
		: msg(msg_from_name(name))
	{}

	virtual const char* what() const noexcept
	{
		return msg.c_str();
	}

	std::string msg;
};

image_channel
channel_from_name(std::string const name)
{
	if (name == "r") {
		return image_channel::red;
	}

	if (name == "g") {
		return image_channel::green;
	}

	if (name =="b") {
		return image_channel::blue;
	}

	if (name == "a") {
		return image_channel::alpha;
	}

	throw channel_name_error(name);
}

uint8_t
channel_value(const png::image &img,
              image_channel const channel,
              uint32_t const row,
              uint32_t const col)
{
	uint32_t const pixel = img.data.at(col + img.width * row);

	switch (channel) {
	case image_channel::red:
		return pixel;
	case image_channel::green:
		return pixel >> 8;
	case image_channel::blue:
		return pixel >> 16;
	case image_channel::alpha:
		return pixel >> 24;
	}

	return 0;
}

void
dump_image(twirr::cmdline::prefix_t const prefix,
           twirr::cmdline::args_t const args)
{
	static char const usage[] = "<layer> <file in>";

	if (args.size() != 2) {
		std::cerr << "Invalid number of parameters to "
		          << prefix << std::endl
		          << "usage: " << prefix << " "
		          << usage << std::endl;
		return;
	}

	auto const chn = channel_from_name(args[0]);
	auto const img = png::load_png(args[1].c_str());

	for (uint32_t row = 0; row < img.height; ++row) {
		for (uint32_t col = 0; col < img.width; ++col) {
			uint8_t const byte = channel_value(img, chn, row, col);
			char buf[3];
			std::snprintf(buf, sizeof buf, "%02x", byte);
			std::cout << " " << buf;
		}
		std::cout << std::endl;
	}

	return;
}

void
readwrite_image(twirr::cmdline::prefix_t const prefix,
                twirr::cmdline::args_t const args)
{
	static char const usage[] = "<file in> <file out>";

	if (args.size() != 2) {
		std::cerr << "Invalid number of parameters to "
		          << prefix << std::endl
		          << "usage: " << prefix << " "
		          << usage << std::endl;
		return;
	}

	auto const img = png::load_png(args[0].c_str());
	png::save_png(args[1].c_str(), img);

	return;
}

void
kannala_accuracy(twirr::cmdline::prefix_t const prefix,
                 twirr::cmdline::args_t const args)
{
	using twirl_tools::model_from_file_at;

	static char const usage[] =
	"<model file> <width> <height> <out-file>";

	if (args.size() != 4) {
		std::cerr << "Invalid number of parameters to "
		          << prefix << std::endl
		          << "usage: " << prefix << " "
		          << usage << std::endl;
		return;
	}

	auto const model = model_from_file_at(args[0]);
	auto const width = std::atoi(args[1].c_str());
	auto const height = std::atoi(args[2].c_str());
	auto const model_fname = args[3];

	auto const result = twirr::accuracy::estimate(model, width, height);

	auto const pixbuf = twirr::accuracy::reformat_offsets(
		result.offset_map,
		result.offset_max
	);

	png::image const img (
		result.width,
		result.height,
		pixbuf.data()
	);

	png::save_png(model_fname.c_str(), img);
	std::cout << "results: " << std::endl;
	std::cout << " offset_max: " << result.offset_max << std::endl;
	std::cout << " polynomial max error: " << result.polynomial_err_max
	          << std::endl;

	return;
}

#endif /* TWIRL_USE_LIBPNG */

int
mmain2(const char * const * const argv)
{
	std::vector<twirr::cmdline::subcmd> cmds;
	cmds.emplace_back("show-model", show_model);
	cmds.emplace_back("estimate-max-theta", estimate_max_theta);
#ifdef TWIRL_USE_LIBPNG
	cmds.emplace_back("reproject-image", reproject_image);
	cmds.emplace_back("dump-image", dump_image);
	cmds.emplace_back("readwrite-image", readwrite_image);
	cmds.emplace_back("kannala-accuracy", kannala_accuracy);
#endif

	auto const cmd = twirr::cmdline::subcmd_cmd(cmds);

	return twirr::cmdline::run(argv, cmd);
}

} /* namespace */

int
main(int, char **argv)
{
	try {
		return mmain2(argv);
	} catch (const twirr::cmdline::subcmd_name_error &e) {
		std::cerr << e.usage << std::endl;
		return 2;
	} catch (const std::exception &e) {
		std::cerr << e.what() << std::endl;
		return 1;
	}
}
