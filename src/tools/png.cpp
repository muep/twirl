/*
Copyright (c) 2014 Joonas Sarajärvi

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/
/*
 * png.cpp
 *
 * Some code that wraps an easier-to-use interface around
 * libpng.
 *
 * This file assumes that libpng is available. The accompanying
 * build system should do some tricks to to avoid compiling this
 * file at all, unless libpng is really available.
 */

#include "png.hpp"

#include <cstdio>
#include <cstdlib>
#include <cstring>

/* From libpng */
#include <png.h>

namespace png {
namespace {

void
read_png_with_libpng(const char * const path,
                     uint32_t *&data_out,
                     uint32_t &width_out,
                     uint32_t &height_out)
{
	/* Default outputs, for the case where we end up
	 * failing the read. */
	data_out = nullptr;
	width_out = 0;
	height_out = 0;

	/* All local variable definitions, which can not be placed
	 * later due to the use of goto. */
	std::FILE *file = nullptr;
	png_structp png = nullptr;
	png_infop info = nullptr;

	uint8_t header[8];
	size_t header_bytes = 0;
	uint32_t width = 0;
	uint32_t height = 0;
	png_bytep *row_ptrs = nullptr;

	file = std::fopen(path, "rb");
	if (file == nullptr) {
		goto end;
	}

	header_bytes = std::fread(header, 1, 8, file);
	if (header_bytes != 8) {
		goto end;
	}

	if (png_sig_cmp(header, 0, 8)) {
		goto end;
	}

	png = png_create_read_struct(PNG_LIBPNG_VER_STRING, 0, 0, 0);
	if (png == nullptr) {
		goto end;
	}

	info = png_create_info_struct(png);
	if (info == nullptr) {
		goto end;
	}

	if (setjmp(png_jmpbuf(png))) {
		/* If setjmp returns non-zero, it means that we came
		 * here through the longjmp based exception mechanism
		 * instead of as part of normal operation. */
		goto end;
	}

	png_init_io(png, file);
	png_set_sig_bytes(png, 8);
	png_read_info(png, info);

	width = png_get_image_width(png, info);
	height = png_get_image_height(png, info);

	png_set_add_alpha(png, 0xff, PNG_FILLER_AFTER);
	png_set_keep_unknown_chunks(png, PNG_HANDLE_CHUNK_NEVER, 0, 0);
	png_set_interlace_handling(png);

	row_ptrs = static_cast<png_bytep *>(
		std::malloc(height * (sizeof *row_ptrs))
	);
	for (uint32_t row = 0; row < height; ++row) {
		row_ptrs[row] = static_cast<png_bytep>(
			std::malloc(width * 4)
		);
	}

	png_read_image(png, row_ptrs);

	width_out = width;
	height_out = height;

	data_out = static_cast<uint32_t*>(
		std::malloc((sizeof *data_out) * width * height)
	);
	for (uint32_t row = 0; row < height; ++row) {
		uint32_t const pos = row * width;
		size_t const byte_cnt = (sizeof *data_out) * width;

		std::memmove(data_out + pos, row_ptrs[row], byte_cnt);
	}

	/* Cleanup of all resources reserved above, in reverse
	 * order. */
end:
	if (row_ptrs != nullptr) {
		for (uint32_t n = 0; n < height; ++n) {
			std::free(row_ptrs[n]);
		}
		std::free(row_ptrs);
		row_ptrs = nullptr;
	}

	if (png != 0) {
		png_destroy_read_struct(&png, &info, 0);
		png = nullptr;
		info = nullptr;
	}

	if (file != nullptr) {
		std::fclose(file);
		file = 0;
	}
} /* read_png_with_libpng */

void
write_png_with_libpng(const char * const path,
                      const image &img)
{
	std::FILE *file = nullptr;
	png_structp png = nullptr;
	png_infop info = nullptr;
	png_bytepp row_ptrs = nullptr;
	uint32_t *data_copy = nullptr;

	file = std::fopen(path, "wb");
	if (file == nullptr) {
		goto end;
	}

	png = png_create_write_struct(
		PNG_LIBPNG_VER_STRING,
		nullptr,
		nullptr,
		nullptr
	);
	if (png == nullptr) {
		goto end;
	}

	info = png_create_info_struct(png);
	if (info == nullptr) {
		goto end;
	}

	if (setjmp(png_jmpbuf(png))) {
		goto end;
	}

	png_init_io(png, file);

	png_set_IHDR(png, info,
		img.width,
		img.height,
		/* bit depth */
		8,
		PNG_COLOR_TYPE_RGB_ALPHA,
		PNG_INTERLACE_NONE,
		PNG_COMPRESSION_TYPE_DEFAULT,
		PNG_FILTER_TYPE_DEFAULT
	);

	/* Let's take a copy of the whole image data, because
	 * for now I have no idea about if libpng will want to
	 * modify it or not. */
	data_copy = static_cast<uint32_t *>(
		std::malloc((sizeof *data_copy) * img.data.size())
	);
	std::memmove(data_copy, img.data.data(),
	             (sizeof *data_copy) * img.data.size());

	/* Then set up a libpng style row pointer array */
	row_ptrs = static_cast<png_bytep *>(
		std::malloc((sizeof *row_ptrs) * img.height)
	);
	for (uint32_t row = 0; row < img.height; ++row) {
		/* We can just point into the middle of
		 * large data chunk. */
		uint32_t const pos = img.width * row;

		row_ptrs[row] = reinterpret_cast<png_bytep>(
			data_copy + pos
		);
	}

	png_set_rows(png, info, row_ptrs);
	png_write_png(png, info, 0, nullptr);

end:
	if (png != nullptr) {
		png_infopp const infopp = info ? &info : nullptr;
		png_destroy_write_struct(&png, infopp);
	}

	if (row_ptrs != nullptr) {
		std::free(row_ptrs);
		row_ptrs = nullptr;
	}

	if (data_copy != nullptr) {
		std::free(data_copy);
		data_copy = nullptr;
	}

	if (file != nullptr) {
		std::fclose(file);
		file = nullptr;
	}
}

std::vector<uint32_t>
data_from_size_and_ptr(uint32_t const size,
                       const uint32_t * const data)
{
	std::vector<uint32_t> result;

	if (size == 0) {
		return result;
	}
	if (data == nullptr) {
		return result;
	}

	result.insert(result.end(), data, data + size);
	return result;
}

} /* namespace */

image::image(uint32_t const width,
             uint32_t const height,
             const uint32_t * const data)
	: width(width),
	  height(height),
	  data(data_from_size_and_ptr(width * height, data))
{
}

image::~image()
{
}

image
load_png(const char * const path)
{
	uint32_t width;
	uint32_t height;
	uint32_t *data;

	read_png_with_libpng(path, data, width, height);

	if (data == nullptr) {
		return image(0, 0, nullptr);
	}

	image result(width, height, data);
	std::free(data);
	return result;
}

void
save_png(const char * const path,
         const image & img)
{
	write_png_with_libpng(path, img);
}

} /* namespace png */
