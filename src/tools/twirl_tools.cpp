/*
Copyright (c) 2014 Joonas Sarajärvi

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/
#include "twirl_tools.hpp"

#include <fstream>
#include <future>
#include <iostream>
#include <sstream>

#include <twirr/parallel_reproject.hpp>

#include "twirl_config.hpp"

namespace twirl_tools {

namespace detail {
namespace {

std::string
text_of_distortion(std::string const name,
                   const twirl::kannala::asym_distortion &d)
{
	std::stringstream out;

	out << name << " distortion:" << std::endl;
	out << " for theta: " << d.d_th[0] << std::endl;
	out << "            " << d.d_th[1] << std::endl;
	out << "            " << d.d_th[2] << std::endl;
	out << " for phi:   " << d.d_ph[0] << std::endl;
	out << "            " << d.d_ph[1] << std::endl;
	out << "            " << d.d_ph[2] << std::endl;
	out << "            " << d.d_ph[3] << std::endl;

	return out.str();
}

} /* namespace */
} /* namespace detail */

model_load_failure::model_load_failure(std::string const reason)
	: reason(reason)
{}

model_load_failure::~model_load_failure() noexcept
{}

const char *
model_load_failure::what() const noexcept
{
	return reason.c_str();
}

twirl::kannala::full_model
model_from_file_at(std::string const path)
{
	auto const contents = whole_file_from(path);

	auto const expected_size = sizeof(float) * 23;
	auto const actual_size = contents.size();

	if (actual_size != expected_size) {
		std::stringstream txt;

		txt << "Expected to get " << expected_size
		    << " bytes from " << path
		    << " but got " << actual_size
		    << " bytes instead.";

		throw model_load_failure(txt.str());
	}

	const std::uint8_t * const bytes = &contents[0];

	return twirl::kannala::model_from_bytes(bytes);

}

std::string
text_of_model(const twirl::kannala::full_model &m)
{
	using detail::text_of_distortion;

	std::stringstream out;

	out << "Symmetric part:" << std::endl;
	for (int n = 0; n < 5; ++n) {
		out << "  k" << n + 1 << ": " << m.symmetric_part.k[n]
		    << std::endl;
	}

	out << text_of_distortion("Tangential", m.tangential_distortion);
	out << text_of_distortion("Radial", m.radial_distortion);

	out << "Affine part:" << std::endl;
	out << "  mu: " << m.affine_part.mu << std::endl;
	out << "  mv: " << m.affine_part.mv << std::endl;
	out << "  u0: " << m.affine_part.u0 << std::endl;
	out << "  v0: " << m.affine_part.v0 << std::endl;

	return out.str();
}

std::vector<std::uint8_t>
whole_file_from(std::string const path)
{
	std::ifstream in(path, std::ios::binary);

	/* Seek to end to find out how big the file is. */
	in.seekg(0, std::ios::end);
	auto const in_len = in.tellg();

	if (in_len <= 0) {
		return std::vector<std::uint8_t>();
	}

	/* Back to beginning */
	in.seekg(0, std::ios::beg);

	/* This is somewhat unprepared for file shrinking
	 * during the read. Quite likely the function will return
	 * some garbage in the vector if that happens. */
	std::vector<std::uint8_t> result(in_len);
	in.read(reinterpret_cast<char*>(result.data()), in_len);

	return result;
}

#ifdef TWIRL_USE_THREADS

void
build_lut(const twirl::kannala::full_model& mdl,
          uint16_t const width,
          uint16_t const height,
          twirl::reproject::lut_entry* const target)
{
	twirr::parallel_reproject::build_lut_parallel(
		mdl,
		width,
		height,
		target
	);
}

#else /* TWIRL_USE_THREADS */

void
build_lut(const twirl::kannala::full_model& mdl,
          uint16_t const width,
          uint16_t const height,
          twirl::reproject::lut_entry* const target)
{
	twirl::reproject::build_lut(mdl, width, height, target);
}


#endif /* TWIRL_USE_THREADS */

} /* namespace twirl_tools */
