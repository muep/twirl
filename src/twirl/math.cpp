/*
Copyright (c) 2014 Joonas Sarajärvi

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/
#include "twirl/math.hpp"

namespace twirl {
namespace math {

sinish::sinish()
{
	auto const steps =
	static_cast<unsigned int>(sizeof(quarter) / sizeof(quarter[0]));

	auto const interval = (pi / 2) / steps;

	for (unsigned int n = 0; n < steps; ++n) {
		auto const x = n * interval;
		quarter[n] = sinr(x);
	}
}

real_t
sinish::cos(real_t const x) const
{
	return this->sin(x + pi / 2);
}

real_t
sinish::sin(real_t const x) const
{
	auto const steps =
	static_cast<unsigned int>(sizeof(quarter) / sizeof(quarter[0]));

	auto const interval = (pi / 2) / steps;

	/* Here is is quite important that the truncation to int
	 * works well.
	 *
	 * The conversion directly from real_t to unsigned int
	 * seemed to provide unexpected results, at least on
	 * GCC 4.8 on the armv7hl Fedora 20. Doing it in steps
	 * and going through int seems to produce working results.
	 */
	auto const floord_float = floorr(x / interval);
	auto const floord_int = static_cast<int>(floord_float);
	auto const floord = static_cast<unsigned int>(floord_int);

	auto const pos = floord % (4 * steps);

	if (pos < steps) {
		return quarter[pos];
	} else if (pos < (steps * 2)) {
		return quarter[steps * 2 - 1 - pos];
	} else if (pos < (steps * 3)) {
		return -quarter[pos - steps * 2];
	} else {
		return -quarter[steps * 4 - 1 - pos];
	}
}

} /* namespace math */
} /* namespace twirl */
