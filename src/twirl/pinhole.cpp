/*
Copyright (c) 2014 Joonas Sarajärvi

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/
#include "twirl/pinhole.hpp"

#include "twirl/kannala.hpp"
#include "twirl/math.hpp"

namespace twirl {
namespace pinhole {

ray_direction
backproject(const model &mdl, vec2 const pxpos)
{
	auto const pos = affine::backward(mdl.affine_part, pxpos);
	auto const r = math::sqrtr(pos.x * pos.x + pos.y * pos.y);

	auto const theta = math::atanr(r / mdl.focal_length);
	auto const phi = math::atan2r(pos.y, pos.x);

	return ray_direction(theta, phi);
}

vec2
project(const model &mdl, ray_direction const rd)
{
	real_t const rf = math::tanr(rd.theta()) * mdl.focal_length;
	vec2 const pos = {
		rf * math::cosr(rd.phi()),
		rf * math::sinr(rd.phi())
	};

	return affine::forward(mdl.affine_part, pos);
}

model
model_for_fov_and_size(real_t const fov,
                       uint16_t const width,
                       uint16_t const height)
{
	auto const bigger =
	static_cast<real_t>(width >= height ? width : height);

	model result;

	/* In both directions, the pixel density is same.
	 * It gets picked so that in the bigger dimension,
	 * extreme values map to edges. */
	result.affine_part.mu = bigger / 2;
	result.affine_part.mv = bigger / 2;

	/* Zero angle gets to center of area. */
	result.affine_part.u0 = static_cast<real_t>(width) / 2;
	result.affine_part.v0 = static_cast<real_t>(height) / 2;

	result.focal_length = 1.0f / math::tanr(fov / 2);

	return result;
}

} /* namespace pinhole */
} /* namespace twirl */
