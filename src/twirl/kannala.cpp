/*
Copyright (c) 2014 Joonas Sarajärvi

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/
#include "twirl/kannala.hpp"

#include "twirl/array.hpp"
#include "twirl/math.hpp"
#include "twirl/matrix.hpp"
#include "twirl/polynomy.hpp"
#include "twirl/pxgrid.hpp"
#include "twirl/vec2_ops.hpp"

namespace twirl {
namespace kannala {

namespace detail {

/**
 * Some quirk in Visual Studio wants this to be added, or else
 * the things in the other anonymous namespace will not get
 * resolved at all places.
 */
namespace {}

namespace {

matrix<2,1>
m21_from_col(vec2 const col)
{
	real_t const values[] = { col.x, col.y };
	return matrix<2,1>(values);
}

/**
 * Get a matrix<2,2> from two vec2 objects.
 */
matrix<2,2>
m22_from_rows(vec2 const row0,
              vec2 const row1)
{
	real_t const values[] = {
		row0.x, row0.y,
		row1.x, row1.y
	};
	return matrix<2,2>(values);
}

/**
 * Convert a 2,1 matrix into a vec2.
 */
vec2
vec2_from_m21(matrix<2,1> mx)
{
	vec2 v = { mx[0][0], mx[1][0] };
	return v;
}

/**
 * Expand factors from a polynomial into form that
 * can be used by twirl::polynomial functions.
 *
 * @param model specifies the degree 1,3,5,7,9 factor
 * of the output polynomial.
 *
 * @param deg0 specifies the degree 0 factor of the
 * polynomial.
 */
array<real_t,10>
radius_poly_of_model(const symmetric_model &model,
                     real_t const deg0)
{
	array<real_t,10> result;
	result[0] = deg0;
	result[1] = model.k[0];
	result[2] = 0;
	result[3] = model.k[1];
	result[4] = 0;
	result[5] = model.k[2];
	result[6] = 0;
	result[7] = model.k[3];
	result[8] = 0;
	result[9] = model.k[4];
	return result;
}

/**
 * Get the part of distortion strength that depends on
 * the theta parameter.
 *
 * Since this is a polynomial function, it has been also written to
 * allow getting a value of its derivative instead of the usual
 * value.
 */
real_t
distortion_by_th_amount(const polynomy &p,
                        real_t const th,
                        const real_t * const d_th)
{
	return p.value_135(d_th, th);
}

/**
 * Get the part of distortion strength that depends on
 * the theta parameter.
 *
 * Since this is a polynomial function, it has been also written to
 * allow getting a value of its derivative instead of the usual
 * value.
 */
real_t
distortion_by_th_amount_d1(const polynomy &p,
                           real_t const th,
                           const real_t * const d_th)
{
	/* Only the non-zero coefficients are stored in
	 * the model structure, so the polynomial gets
	 * expanded here. */
	const real_t th_polynomial[] = {
		0.0f,
		d_th[0],
		0.0f,
		d_th[1],
		0.0f,
		d_th[2]
	};
	/* Now it is possible to just use the common polynomial
	 * evaluation function. */
	return p.derivative(th_polynomial, 5, 1, th);
}

/**
 * Compute part of the product in distortion_amount
 * that depends on just the phi angle.
 *
 * @param dir is the ray direction for
 * which distortion amount is being computed.
 *
 * @param d_ph is an array of four real_t numbers
 * that describe how phi affects the asymmetrict distortion.
 */
real_t
distortion_by_ph_amount(raydir_plus2 const &dir,
                        const real_t * const d_ph)
{
	real_t const k1 = d_ph[0] * dir.cos_phi();
	real_t const k2 = d_ph[1] * dir.sin_phi();
	real_t const k3 = d_ph[2] * dir.cos_2phi();
	real_t const k4 = d_ph[3] * dir.sin_2phi();

	real_t result = k1 + k2 + k3 + k4;
	return result;
}

/**
 * First derivative of distortion_by_ph_amount.
 *
 * Beautifully transcribed from an analytic exact derivation.
 * Hopefully it is correct.
 *
 * Parameters are basically same as they are for
 * distortion_by_ph_amount.
 */
real_t
distortion_by_ph_amount_d1(raydir_plus2 const &dir,
                           const real_t * const d_ph)
{
	real_t const k1 = - 1.0f * d_ph[0] * dir.sin_phi();
	real_t const k2 =   1.0f * d_ph[1] * dir.cos_phi();
	real_t const k3 = - 2.0f * d_ph[2] * dir.sin_2phi();
	real_t const k4 =   2.0f * d_ph[3] * dir.cos_2phi();
	real_t const result = k1 + k2 + k3 + k4;
	return result;
}

real_t
distortion_amount(const polynomy &p,
                  const asym_distortion &d,
                  raydir_plus2 const &dir)
{
	/* The output from this function is conveniently
	 * a product of two values, one of which depends on
	 * theta and with the other depending on phi. */

	real_t const from_th =
	distortion_by_th_amount(p, dir.theta(), d.d_th);

	real_t const from_phi =
	distortion_by_ph_amount(dir, d.d_ph);

	return from_th * from_phi;
}

/**
 * Derivative of the distortion_amount function against changing
 * theta in the second parameter.
 */
real_t
distortion_amount_deriv_th(const polynomy &p,
                           const asym_distortion &d,
                           raydir_plus2 const &dir)
{
	real_t const from_theta =
	distortion_by_th_amount_d1(p, dir.theta(), d.d_th);

	/* This is a constant from point of view of derivation,
	 * so it is the same expression as in the distortion_amount
	 * function. */
	real_t const from_phi = distortion_by_ph_amount(
		dir,
		d.d_ph
	);

	return from_theta * from_phi;
}

real_t
distortion_amount_deriv_ph(const polynomy &p,
                           const asym_distortion &d,
                           raydir_plus2 const &dir)
{
	/* Like in distortion_amount, no derivation here */
	real_t const from_th =
	distortion_by_th_amount(p, dir.theta(), d.d_th);

	/* Here, need to take the derivative */
	real_t const from_phi =
	distortion_by_ph_amount_d1(dir, d.d_ph);

	return from_th * from_phi;
}

/**
 * Derivative of twirl::kannala::distortion against changing
 * phi.
 */
vec2
distortion_deriv_ph(const polynomy &p,
                    const asym_distortion & d_rad,
                    const asym_distortion & d_tan,
                    raydir_plus2 const &rdir)
{
	/* Now need to get both the derivatives and the
	 * actual distortion value */
	auto const amount_rad_d = distortion_amount_deriv_ph(p, d_rad, rdir);
	auto const amount_tan_d = distortion_amount_deriv_ph(p, d_tan, rdir);

	auto const amount_rad = distortion_amount(
		p,
		d_rad,
		rdir
	);

	auto const amount_tan = distortion_amount(
		p,
		d_tan,
		rdir
	);

	auto const sin_ph = rdir.sin_phi();
	auto const cos_ph = rdir.cos_phi();

	auto const x_d_rad = -sin_ph * amount_rad + cos_ph * amount_rad_d;
	auto const x_d_tan = -cos_ph * amount_tan - sin_ph * amount_tan_d;

	auto const y_d_rad =  cos_ph * amount_rad + sin_ph * amount_rad_d;
	auto const y_d_tan = -sin_ph * amount_tan + cos_ph * amount_tan_d;

	vec2 o;
	o.x = x_d_rad + x_d_tan;
	o.y = y_d_rad + y_d_tan;
	return o;
}

/**
 * Derivative of twirl::kannala::distortion against
 * changing the theta value of ray direction.
 */
vec2
distortion_deriv_th(const polynomy &p,
                    const asym_distortion &d_rad,
                    const asym_distortion &d_tan,
                    raydir_plus2 const &rdir)
{
	using detail::distortion_amount_deriv_th;

	vec2 const relative = {
		distortion_amount_deriv_th(p, d_rad, rdir),
		distortion_amount_deriv_th(p, d_tan, rdir)
	};

	return rdir.phi_rotated(relative);
}

/**
 * Derivatives of the symmetric projection output for
 * changing the theta value.
 */
vec2
project_symmetric_deriv_th(const polynomy &p,
                           const symmetric_model & mdl,
                           raydir_plus const &rdir)
{
	real_t const deg0 = 0;
	auto const r_poly = detail::radius_poly_of_model(mdl, deg0);

	auto const d_r = p.derivative(r_poly, 9, 1, rdir.theta());

	vec2 result;
	result.x = rdir.cos_phi() * d_r;
	result.y = rdir.sin_phi() * d_r;
	return result;
}

/**
 * Derivatives of the symmetric projection output for
 * changing the phi value.
 */
vec2
project_symmetric_deriv_ph(const polynomy &p,
                           const symmetric_model &mdl,
                           raydir_plus const &rdir)
{
	/* The radius does not depend on phi, so we use just
	 * its usual expression */
	auto const r = p.value_13579(mdl.k, rdir.theta());

	vec2 result;
	result.x = - r * rdir.sin_phi();
	result.y =   r * rdir.cos_phi();
	return result;
}

} /* anonymous namespace */
} /* namespace detail */

full_model
model_from_bytes(const uint8_t * const bytes)
{
	typedef twirl::array<float,23> raw;
	using twirl::math::pi;

	/**
	 * View into the byte array as a fixed size float
	 * array.
	 *
	 * NOTE: This is possibly a quite silly way to interpret
	 * anything.
	 */
	const raw & src = *reinterpret_cast<const raw*>(bytes);

	full_model dst;

	/* TODO: Check if these are right.
	 * These offsets are based on what is in the forwardproj.m
	 * file of Kannala's calibration toolbox. */

	dst.symmetric_part.k[0] = src[0];
	dst.symmetric_part.k[1] = src[1];

	/* Yes, it seems to really say that the order is this weird. */
	dst.symmetric_part.k[2] = src[6];
	dst.symmetric_part.k[3] = src[7];
	dst.symmetric_part.k[4] = src[8];

	/* TODO: Get us a way to compute it here, or just omit it from
	 * the model.
	 *
	 * This seems to be omitted in the model data,
	 * so it is just guessed for now. */
	dst.symmetric_part.th_max = pi / 2;

	dst.affine_part.mu = src[2];
	dst.affine_part.mv = src[3];
	dst.affine_part.u0 = src[4];
	dst.affine_part.v0 = src[5];

	dst.tangential_distortion.d_th[0] = src[9];
	dst.tangential_distortion.d_th[1] = src[10];
	dst.tangential_distortion.d_th[2] = src[11];
	dst.tangential_distortion.d_ph[0] = src[15];
	dst.tangential_distortion.d_ph[1] = src[16];
	dst.tangential_distortion.d_ph[2] = src[17];
	dst.tangential_distortion.d_ph[3] = src[18];

	dst.radial_distortion.d_th[0] = src[12];
	dst.radial_distortion.d_th[1] = src[13];
	dst.radial_distortion.d_th[2] = src[14];
	dst.radial_distortion.d_ph[0] = src[19];
	dst.radial_distortion.d_ph[1] = src[20];
	dst.radial_distortion.d_ph[2] = src[21];
	dst.radial_distortion.d_ph[3] = src[22];

	return dst;
}

real_t
estimated_max_theta(const polynomy &p,
                    const full_model &model,
                    isize const picture_size)
{
	/* To avoid an implicit narrowing conversion later
	 * in this function, these are casted here. */
	auto const w = static_cast<int>(picture_size.w);
	auto const h = static_cast<int>(picture_size.h);

	/* Corner pixels of an image. */
	ivec2 const corners[] = {
		{ 0, 0 },
		{ w - 1, 0 },
		{ w - 1, h - 1 },
		{ 0, h - 1 }
	};

	real_t result = 0;

	for (auto const &ipt : corners) {
		auto const pt = pxgrid::vec2_from_ivec2(ipt);
		auto const dir = backproject(p, model, pt);

		if (dir.theta() > result) {
			result = dir.theta();
		}
	}

	return result;
}

ray_direction
raydir_of_v3(vec3 const pt)
{
	real_t const r = sqrt(pt.x * pt.x + pt.y * pt.y);

	auto const theta = math::atanr(r / (-pt.z));
	auto const phi = math::atan2r(pt.y, pt.x);

	return ray_direction(theta, phi);
}

vec2
project_symmetric(const polynomy &p,
                  const symmetric_model &model,
                  raydir_plus const &dir)
{
	auto const r = p.value_13579(model.k, dir.theta());

	vec2 result;
	result.x = dir.cos_phi() * r;
	result.y = dir.sin_phi() * r;
	return result;
}

vec2
distortion(const polynomy &p,
           const asym_distortion &radial,
           const asym_distortion &tangential,
           raydir_plus2 const &raydir)
{
	using detail::distortion_amount;

	vec2 const relative = {
		distortion_amount(p, radial, raydir),
		distortion_amount(p, tangential, raydir)
	};

	return raydir.phi_rotated(relative);
}

vec2
undistortion(const polynomy &p,
             const full_model& model,
             vec2 const pos)
{
	/* Need these functions of rdir:
	 *
	 * 1) d_delta_u / dtheta
	 * 2) d_delta_v / dtheta
	 * 3) d_delta_u / dphi
	 * 4) d_delta_v / dphi
	 *
	 * 5) d_f_u / dtheta
	 * 6) d_f_v / dtheta
	 * 7) d_f_u / dphi
	 * 8) d_f_v / dphi
	 */

	using detail::distortion_deriv_th;
	using detail::distortion_deriv_ph;
	using detail::project_symmetric_deriv_th;
	using detail::project_symmetric_deriv_ph;
	using detail::m22_from_rows;
	using detail::m21_from_col;
	using detail::vec2_from_m21;

	/* Some shorthand */
	auto const &sym_mdl = model.symmetric_part;
	auto const &rad_d = model.radial_distortion;
	auto const &tan_d = model.tangential_distortion;

	auto const rdir0 =
	static_cast<raydir_plus2>(backproject_symmetric(p, sym_mdl, pos));

	/* 1) and 2) */
	auto const dd_th = distortion_deriv_th(p, rad_d, tan_d, rdir0);

	/* 3) and 4) */
	auto const dd_ph = distortion_deriv_ph(p, rad_d, tan_d, rdir0);

	auto const j_s = m22_from_rows(dd_th, dd_ph);

	/* 5) and 6) */
	auto const fd_th = project_symmetric_deriv_th(p, sym_mdl, rdir0);
	/* 7) and 8) */
	auto const fd_ph = project_symmetric_deriv_ph(p, sym_mdl, rdir0);

	auto const j_f = m22_from_rows(fd_th, fd_ph);
	auto const j_f_inv = matrix2x2::invert(j_f);

	/**
	 * The matrix expression from equation (15). Not sure what this
	 * should be called. */
	auto const thing = matrix<2,2>::identity() + j_s * j_f_inv;

	auto const d0 = m21_from_col(distortion(p, rad_d, tan_d, rdir0));

	auto const shift = thing * d0;

	return vec2_from_m21(shift);
}

vec2
project(const polynomy &p,
        const full_model &model,
        raydir_plus2 const &dir)
{
	vec2 const s = project_symmetric(
		p,
		model.symmetric_part,
		dir
	);

	vec2 const d = distortion(
		p,model.radial_distortion,
		model.tangential_distortion,
		dir
	);

	vec2 const pos = { s.x + d.x, s.y + d.y };

	vec2 const pxpos = affine::forward(model.affine_part, pos);
	return pxpos;
}

ray_direction
backproject_symmetric(const polynomy &p,
                      const symmetric_model &model,
                      vec2 const pt)
{
	real_t const phi = atan2(pt.y, pt.x);
	real_t const r = math::sqrtr(pt.x * pt.x + pt.y * pt.y);

	real_t const deg0 = -r;
	auto const polynomial = detail::radius_poly_of_model(model, deg0);

	unsigned int const order = 9;

	real_t const max_diff = 0.001f;
	real_t const theta = p.root(polynomial, order, max_diff);

	return ray_direction(theta, phi);
}

ray_direction
backproject(const polynomy &p,
            const full_model &model,
            vec2 const px_pos)
{
	/* Some shorthand */
	auto const &ap = model.affine_part;
	auto const &sp = model.symmetric_part;

	auto const distorted_pos = affine::backward(ap, px_pos);

	auto const err = undistortion(p, model, distorted_pos);
	vec2 const pos = {
		distorted_pos.x - err.x,
		distorted_pos.y - err.y
	};

	ray_direction const dir = backproject_symmetric(p, sp, pos);
	return dir;
}

} /* namespace kannala */
} /* namespace twirl */
