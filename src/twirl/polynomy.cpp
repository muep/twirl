/*
Copyright (c) 2014 Joonas Sarajärvi

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/
#include "twirl/polynomy.hpp"

#include <math.h>

namespace twirl {
namespace {

uint32_t
factorial(unsigned int n)
{
	uint32_t result = 1;

	for (unsigned int i = 2; i <= n; ++i) {
		result *= i;
	}

	return result;
}

/**
 * Get the derivation coefficient for a term in a plynomial.
 *
 * For example, if we have the polynomial 1*x^3 + 4*x^2 + 2*x +1;
 *
 * The first derivative of it would be
 *  1*[3]*x^2 + 4*[2]*x + 2*[1]
 *
 * This function computes the factors that are marked with []
 * in the above expression.
 */
real_t
deriv_k(unsigned int deriv_order,
        unsigned int term_order)
{
	unsigned int const u = deriv_order + term_order;
	unsigned int const l = term_order;

	unsigned int const result = factorial(u) / factorial(l);

	return static_cast<real_t>(result);
}

/**
 * Generate a look-up-table of deriv_k output.
 *
 * To avoid repeated runs of deriv_k and factorial
 * functions, this template can be used to generate a
 * look-up table that contains its output for the relatively
 * small set of inputs that are going to be used.
 */
template<uint32_t max_dgr, uint32_t max_deriv>
void
make_deriv_k_lut(real_t * const result)
{
	for (unsigned int deriv_order = 0;
		 deriv_order < max_deriv;
		 ++deriv_order) {
		for (unsigned int term_order = 0;
			 term_order < max_dgr;
			 ++term_order) {
			auto const pos = deriv_order * max_dgr + term_order;
			result[pos] = deriv_k(deriv_order, term_order);
		}
	}
}

/**
 * Get a precached result of deriv_k from a lookup table.
 */
template<unsigned int max_dgr, unsigned int max_deriv>
real_t
deriv_k_lookup(unsigned int const deriv_order,
               unsigned int const term_order,
               const real_t * const lut)
{
	return lut[deriv_order * max_dgr + term_order];
}

/* A somewhat poor implementation of Halley's root finding
 * algorithm. */
twirl::real_t
polynomial_root_step(const polynomy &p,
                     twirl::real_t const * const f,
                     unsigned int const f_order,
                     twirl::real_t const d0,
                     twirl::real_t const x0)
{
	real_t const d1 = p.derivative(f, f_order, 1, x0);
	real_t const d2 = p.derivative(f, f_order, 2, x0);

	return x0 - 2 * d0 * d1 / ( 2 * d1 * d1 - d0 * d2);
}

} /* namespace */

polynomy::polynomy()
{
	/* Remember to change the length of data in d if our array
	 * size changes */
	static_assert(sizeof(d) == 48 * 4,
		          "Size of data cached in polynomy is not consistent");

	make_deriv_k_lut<16,3>(d);
}

real_t
polynomy::derivative(const real_t * const k,
                     uint_fast16_t const poly_order,
                     uint_fast16_t const deriv_order,
                     real_t const x) const
{
	real_t acc = 0;

	/**
	 * This variable tracks the value of the x^pos expression,
	 * inside the loop below. It could also easily be computed
	 * where it is needed, but the current way is way more
	 * efficient than computing it with a separate loop on each
	 * iteration, and also much faster than using the pow function
	 * from the stdlib.
	 */
	real_t x_to_pos = 1.0f;

	/* The i variable tracks [deriv_order, poly_order)
	 * and pos tracks from [0, (poly_order - deriv_order)). */
	for (unsigned int i = deriv_order; i <= poly_order; ++i) {
		uint_fast16_t const pos = i - deriv_order;

		/* Base polynomial factor */
		auto const dk = deriv_k_lookup<16, 3>(deriv_order, pos, d);
		auto const k_i = k[i];
		real_t tmp =  dk * k_i;

		/* Multiplied by x^pos */
		tmp *= x_to_pos;

		acc += tmp;

		x_to_pos *= x;
	}

	return acc;
}

real_t
polynomy::root(const real_t * const poly,
               uint_fast16_t const poly_order,
               real_t const max_diff) const
{
	/* This is a quite bad initial value, especially
	 * in case the function happens to be symmetric. */
	real_t x = 0;

	for (unsigned int i = 0; i < 10; ++i) {
		real_t const d0 = value(
			poly,
			poly_order,
			x
		);

		if (fabs(d0) < max_diff) {
			/* Result is already good enough */
			return x;
		}

		/* Compute the next result, try it */
		x = polynomial_root_step(
			*this,
			poly,
			poly_order,
			d0,
			x
		);
	}

	return 0;

}

real_t
polynomy::value(const real_t * const k,
                uint_fast16_t order,
				real_t const x) const
{
	return derivative(k, order, 0, x);
}

real_t
polynomy::value_13579(const real_t * const k,
                      real_t const x) const
{
	real_t const poly[] = {
		0,
		k[0],
		0,
		k[1],
		0,
		k[2],
		0,
		k[3],
		0,
		k[4]
	};

	return this->derivative(
		poly,
		9,
		0,
		x
	);
}

real_t
polynomy::value_135(const real_t * const k,
                   real_t const x) const
{
	real_t const poly[] = {
		0,
		k[0],
		0,
		k[1],
		0,
		k[2]
	};

	return this->derivative(
		poly,
		5,
		0,
		x
	);
}

} /* namespace twirl */
