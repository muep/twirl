/*
Copyright (c) 2014 Joonas Sarajärvi

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/
#include "twirl/reproject.hpp"

#include "twirl/kannala.hpp"
#include "twirl/pinhole.hpp"
#include "twirl/polynomy.hpp"
#include "twirl/pxgrid.hpp"

#include "twirl_config.hpp"

namespace twirl {
namespace reproject {
namespace detail {

#ifdef _MSC_VER
namespace {}
#endif

namespace {

struct rect {
	int x;
	int y;
	int w;
	int h;

	rect(int x,
	     int y,
	     int w,
	     int h)
	  : x(x), y(y), w(w), h(h)
	{}

	bool
	contains(int const ox, int const oy) const
	{
		if (ox < x) {
			return false;
		}
		if (ox >= x + w) {
			return false;
		}
		if (oy < y) {
			return false;
		}
		if (oy >= y + h) {
			return false;
		}
		return true;
	}
};

lut_entry
invalid_entry()
{
	lut_entry result;
	result.offset = 0xffffffff;
	return result;
}

bool
is_invalid(lut_entry const entry)
{
	return entry.offset == 0xffffffff;
}

lut_entry
entry_from_ivec(ivec2 const pos, uint16_t const width)
{
	lut_entry result;

	result.offset = width * pos.y + pos.x;

	return result;
}

} /* namespace */
} /* namespace detail */

void
partial(const kannala::full_model &km,
       uint32_t const width,
       uint32_t const height,
       uint32_t const black,
       const uint32_t * const src,
       uint32_t * const dest,
       uint32_t const from_row,
       uint32_t const to_row)
{
	using twirl::pxgrid::ivec2_rounded_from_vec2;
	using twirl::pxgrid::vec2_from_ivec2;

	twirl::isize const sz = {
		static_cast<unsigned int>(width),
		static_cast<unsigned int>(height)
	};

	detail::rect const src_rect(0, 0, width, height);

	polynomy const p;

	auto const max_theta = kannala::estimated_max_theta(p, km, sz);

	auto const pm =
	pinhole::model_for_fov_and_size(2 * max_theta, width, height);

	for (uint32_t row = from_row; row < to_row; ++row) {
		for (uint32_t col = 0; col < width; ++col) {
			twirl::ivec2 const dest_px = {
				static_cast<int>(col),
				static_cast<int>(row)
			};

			twirl::vec2 const dest_pos = vec2_from_ivec2(dest_px);

			auto const dir =
			pinhole::backproject(pm, dest_pos);

			auto const src_pos = kannala::project(
				p,
				km,
				static_cast<kannala::raydir_plus2>(dir)
			);

			auto const src_px = ivec2_rounded_from_vec2(src_pos);

			auto const px =
			src_rect.contains(src_px.x, src_px.y) ?
				src[width * src_px.y + src_px.x] :
				black;

			dest[width * row + col] = px;
		}
	}
}

void
whole(const kannala::full_model &fm,
      uint32_t const width,
      uint32_t const height,
      uint32_t const black,
      const uint32_t * const data_in,
      uint32_t * const data_out)
{
	uint32_t const from_row = 0;
	uint32_t const to_row = height;

	partial(
		fm,
		width,
		height,
		black,
		data_in,
		data_out,
		from_row,
		to_row
	);
}

void
build_partial_lut(const kannala::full_model &km,
                     uint32_t const width,
                     uint32_t const height,
                     lut_entry * const target,
                     uint32_t const from_row,
                     uint32_t const to_row)
{
	using twirl::pxgrid::ivec2_rounded_from_vec2;
	using twirl::pxgrid::vec2_from_ivec2;

	twirl::isize const sz = {
		static_cast<unsigned int>(width),
		static_cast<unsigned int>(height)
	};

	detail::rect const src_rect(0, 0, width, height);

	polynomy const p;

	auto const max_theta = kannala::estimated_max_theta(p, km, sz);

	auto const pm =
	pinhole::model_for_fov_and_size(2 * max_theta, width, height);

	uint32_t tgt_offset = width * from_row;
	for (uint32_t row = from_row; row < to_row; ++row) {
		for (uint32_t col = 0; col < width; ++col) {
			twirl::ivec2 const dest_px = {
				static_cast<int>(col),
				static_cast<int>(row)
			};

			auto const dest_pos = vec2_from_ivec2(dest_px);

			auto const dir =
			pinhole::backproject(pm, dest_pos);

			auto const src_pos = kannala::project(
				p,
				km,
				static_cast<kannala::raydir_plus2>(dir)
			);

			auto const src_px = ivec2_rounded_from_vec2(src_pos);

			lut_entry const le =
			src_rect.contains(src_px.x, src_px.y) ?
			detail::entry_from_ivec(src_px, width) :
			detail::invalid_entry();

			target[tgt_offset] = le;
			++tgt_offset;
		}
	}

}

void
build_lut(const kannala::full_model &km,
          uint32_t const width,
          uint32_t const height,
          lut_entry * target)
{
	build_partial_lut(km, width, height, target, 0, height);
}

void
correct_with_lut(const lut_entry *const lut,
                 uint32_t const from_pos,
                 uint32_t const to_pos,
                 uint32_t const black,
                 const uint32_t * const data_in,
                 uint32_t * const data_out)
{
	for (uint32_t pos = from_pos; pos < to_pos; ++pos) {
		auto const entry = lut[pos];

#ifdef TWIRL_USE_ARM_TWEAKS
		{
			int const pld_offset = 7;
			const void * const p = lut + (pos + pld_offset);
			asm(
				"pld [%[foo]]" /* asm */:
				: /* output operands */
				[foo] "r" (p) : /* input operands */
				/* clobber list */
			);
		}
#endif

		auto const color =
			detail::is_invalid(entry) ?
			black :
			data_in[entry.offset];

		data_out[pos] = color;
	}
}


} /* namespace reproject */
} /* namespace twirl */
