/*
Copyright (c) 2014 Joonas Sarajärvi

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/
/*
 * ocv_test.cpp
 *
 * A simple program that takes a list of image file names
 * as its command line parameters. The program will then
 * try to feed the given images to the checkerboard corner
 * finder function and extract corner locations from them.
 *
 * The images are then shown with corner locations overlaid
 * on them. This is useful for verifying that the OpenCV
 * function correctly finds the corner locations.
 */

#include <iostream>

#include <opencv2/opencv.hpp>
#include <opencv2/calib3d/calib3d.hpp>


namespace {

void
display_corners_from(const char * const filename)
{
	auto img = cv::imread(filename);
	if (img.empty()) {
		std::cerr << "Could not read " << filename << std::endl;
	}

	cv::Size const grid_size(12, 8);
	std::vector<cv::Point2f> points;

	bool const did_find =
	cv::findChessboardCorners(img, grid_size, points);

	cv::Mat augmented = img;
	cv::drawChessboardCorners(augmented, grid_size, points, did_find);
	cv::imshow(filename, augmented);

	cv::waitKey(0);
	cv::destroyAllWindows();
}

} /* namespace */

int main(int argc, char **argv)
{
	if (argc < 2) {
		std::cerr << "Usage: " << argv[0]
		          << " <filename>" << std::endl;
		return 1;
	}

	for (int n = 1; n < argc; ++n) {
		const char * const filename = argv[n];
		display_corners_from(filename);
	}

	return 0;
}
