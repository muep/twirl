/*
Copyright (c) 2014 Joonas Sarajärvi

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/

#include "twirr/parallel_reproject.hpp"

#include <future>
#include <thread>
#include <vector>

#include "twirl/kannala.hpp"
#include "twirl/reproject.hpp"

namespace twirr {
namespace parallel_reproject {

namespace {
namespace detail {

int
pick_thread_count()
{
	auto const hw_cc = std::thread::hardware_concurrency();

	return hw_cc == 0 ? 2 : hw_cc;
}

} /* namespace detail */
} /* namespace */

void
parallel(const twirl::kannala::full_model& fm,
         uint32_t const width,
         uint32_t const height,
         uint32_t const black,
         const uint32_t * const data_in,
         uint32_t* const data_out)
{
	auto const thread_count = detail::pick_thread_count();

	std::vector<std::future<void>> futures;
	futures.reserve(thread_count);

	for (int t = 0; t < thread_count; ++t) {
		uint32_t const start = (t * height) / thread_count;
		uint32_t const end = ((t + 1) * height) / thread_count;

		auto f = std::async(
			std::launch::async,
			twirl::reproject::partial,
			fm,
			width,
			height,
			black,
			data_in,
			data_out,
			start,
			end
		);

		futures.emplace_back(std::move(f));
	}

	for (auto &f : futures) {
		f.get();
	}
}

void
build_lut_parallel(const twirl::kannala::full_model& m,
                   uint32_t const width,
                   uint32_t const height,
                   twirl::reproject::lut_entry* target)
{
	auto const thread_count = detail::pick_thread_count();

	std::vector<std::future<void>> futures;
	futures.reserve(thread_count);

	for (int t = 0; t < thread_count; ++t) {
		uint32_t const start = (t * height) / thread_count;
		uint32_t const end = ((t + 1) * height) / thread_count;

		auto f = std::async(
			std::launch::async,
			twirl::reproject::build_partial_lut,
			m,
			width,
			height,
			target,
			start,
			end
		);

		futures.emplace_back(std::move(f));
	}
	for (auto &f : futures) {
		f.get();
	}
}

void
with_lut_parallel(const twirl::reproject::lut_entry* lut,
                  uint32_t const size,
                  uint32_t const black,
                  const uint32_t* const image_in,
                  uint32_t* const image_out)
{
	auto const thread_count = detail::pick_thread_count();

	std::vector<std::future<void>> futures;
	futures.reserve(thread_count);

	for (int t = 0; t < thread_count; ++t) {
		uint32_t const start = (t * size) / thread_count;
		uint32_t const end = ((t + 1) * size) / thread_count;

		auto f = std::async(
			std::launch::async,
			twirl::reproject::correct_with_lut,
			lut,
			start,
			end,
			black,
			image_in,
			image_out
		);

		futures.emplace_back(std::move(f));
	}
	for (auto &f : futures) {
		f.get();
	}
}

} /* namespace reproject */
} /* namespace twirr */
