/*
Copyright (c) 2014 Joonas Sarajärvi

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/
#include "cmdline.hpp"

#include <iostream>
#include <sstream>
#include <string>
#include <vector>

namespace twirr {
namespace cmdline {

namespace {

bool
is_help(arg_t const txt)
{
	if (txt == "-h") {
		return true;
	}

	if (txt == "--help") {
		return true;
	}

	return false;
}

subcmd
search_subcmd(std::vector<subcmd> const cmds,
              name_t const name)
{
	for (auto sc: cmds) {
		if (sc.name == name) {
			return sc;
		}
	}

	return subcmd();
}

args_t
unpack_argv(const char * const * argv)
{
	args_t result;

	while (*argv != nullptr) {
		result.emplace_back(*argv);
		argv++;
	}

	return result;
}

struct subcmd_functor {
	subcmd_functor(std::vector<subcmd> subcmds)
		: subcmds(subcmds)
	{}

	void
	operator()(prefix_t prefix,
	           args_t args) const;

	std::string
	usage(const char * prefix) const;

	std::vector<subcmd> subcmds;
};

void
subcmd_functor::operator()(prefix_t const prefix,
                           args_t const args) const
{
	if (args.empty()) {
		throw subcmd_name_error(usage(prefix.c_str()));
	}

	auto const arg = args[0];

	if (is_help(arg)) {
		std::cout << usage(prefix.c_str());
		return;
	}

	auto const r = search_subcmd(subcmds, arg);
	if (r.valid()) {
		args_t const args2(args.begin() + 1, args.end());
		auto const prefix2 = prefix + " " + arg;
		r.action(prefix2, args2);
		return;
	}

	throw subcmd_name_error(usage(prefix.c_str()));
}

std::string
subcmd_functor::usage(const char* const prefix) const
{
	std::stringstream o(std::ios::out);

	o << "usage: " << prefix << " <subcomand> [--help]|.." << std::endl;
	o << "available subcommands:" << std::endl;

	for (auto sc: subcmds) {
		o << " " << sc.name << std::endl;
	}

	return o.str();
}



} /* namespace */

subcmd::subcmd(const name_t name,
               const action_t action)
	: name(name),
	  action(action)
{
}

subcmd::subcmd()
{
}

bool subcmd::valid() const
{
	return bool(action);
}

subcmd_name_error::subcmd_name_error(std::string const usage)
	: usage(usage)
{
}

const char* subcmd_name_error::what() const noexcept
{
    return "twirr::cmdline::subcmd_name_error";
}

subcmd_name_error::~subcmd_name_error()
{
}

name_t
fixed_name(const char* const target_name)
{
	return target_name;
}

action_t
subcmd_cmd(std::vector<subcmd> const subcmds)
{
	return subcmd_functor(subcmds);
}

int
run(const char*const* argv, action_t action)
{
	try {
		action(argv[0], unpack_argv(argv + 1));
		return 0;
	} catch (subcmd_name_error &) {
		throw;
	} catch (const std::exception &e) {
		std::cerr << e.what() << std::endl
		          << "exiting." << std::endl;
		return 1;
	}
}

} /* namespace cmdline */
} /* namespace twirr */
