/*
Copyright (c) 2014 Joonas Sarajärvi

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/
#include "accuracy.hpp"

#include <cmath>

#include "twirl/math.hpp"
#include "twirl/polynomy.hpp"

namespace twirr {
namespace accuracy {

namespace {

twirl::real_t
v2diff(twirl::vec2 const v0,
       twirl::vec2 const v1)
{
	auto const dx = v0.x - v1.x;
	auto const dy = v0.y - v1.y;

	return twirl::math::sqrtr(dx * dx + dy * dy);
}

std::uint8_t
u8_from_real(twirl::real_t const x, twirl::real_t const max)
{
	if (x <= 0.0f) {
		return 0;
	}
	if (x >= max) {
		return 255;
	}

	return static_cast<std::uint8_t>(255 * x / max);
}

} /* namespace */

result_t
estimate(const twirl::kannala::full_model &model,
         std::uint16_t const width,
         std::uint16_t const height)
{
	using twirl::kannala::backproject;
	using twirl::kannala::project;

	result_t result;
	result.width = width;
	result.height = height;
	result.model = model;
	result.offset_map.reserve(width * height);
	result.polynomial_err_max = 0.0f;

	twirl::polynomy const polynomy;

	for (std::uint16_t row = 0; row < height; ++row) {
		for (std::uint16_t col = 0; col < width; ++col) {
			using twirl::kannala::raydir_plus2;

			twirl::vec2 const pos0 = {
				static_cast<twirl::real_t>(col),
				static_cast<twirl::real_t>(row)
			};
			auto const rd = backproject(
				polynomy,
				model,
				pos0
			);

			auto const pos1 = project(
				polynomy,
				model,
				raydir_plus2(rd)
			);

			auto const offset = v2diff(pos0, pos1);
			result.offset_map.push_back(offset);

			result.offset_max =
				std::fmax(offset, result.offset_max);
		}
	}

	twirl::isize const size = { width, height };

	auto const max_theta = twirl::kannala::estimated_max_theta(
		polynomy,
		model,
		size
	);

	for (int n = 0; n < 1000; ++n) {
		twirl::real_t const theta = max_theta * (n / 1000.0f);
		twirl::ray_direction const rd0(theta, 0.0f);

		auto const pos = twirl::kannala::project_symmetric(
			polynomy,
			model.symmetric_part,
			static_cast<twirl::kannala::raydir_plus>(rd0)
		);

		auto const thetaish = twirl::kannala::backproject_symmetric(
			polynomy,
			model.symmetric_part,
			pos
		).theta();

		auto const dif = std::abs(theta - thetaish);

		result.polynomial_err_max =
			std::fmax(result.polynomial_err_max, dif);
	}

	return result;
}

std::vector<std::uint32_t>
reformat_offsets(std::vector<twirl::real_t> const offsets,
                 twirl::real_t const max)
{
	std::vector<std::uint32_t> result;
	result.reserve(offsets.size());

	for (auto offset: offsets) {
		std::uint8_t const lum = u8_from_real(offset, max);
		std::uint8_t const alpha = 255;

		/* TODO: stop neglecting the endianness
		 * details. */
		std::uint32_t const val = (
			alpha << 24 |
			lum << 16 |
			lum << 8 |
			lum
		);
		result.push_back(val);
	}

	return result;
}

} /* namespace accuracy */
} /* namespace twirr */
