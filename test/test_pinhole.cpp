/*
Copyright (c) 2014 Joonas Sarajärvi

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/
#include "twirl/array.hpp"
#include "twirl/math.hpp"
#include "twirl/pinhole.hpp"

#include "twirr/testdata.hpp"

#include "twirl_test.hpp"

twirl::ray_direction
raydir_of_v3(twirl::vec3 const pt)
{
	using twirl::math::atanr;
	using twirl::math::atan2r;
	using twirl::math::sqrtr;

	auto const r = sqrtr(pt.x * pt.x + pt.y * pt.y);

	auto const theta = atanr(r / (-pt.z));
	auto const phi = atan2r(pt.y, pt.x);

	return twirl::ray_direction(theta, phi);
}

twirl::pinhole::model
example_model(void)
{
	twirl::pinhole::model mdl;

	mdl.focal_length = 1.0f;

	/* Something like a 1024 x 1024 pixel sensor */
	mdl.affine_part.mu = 512;
	mdl.affine_part.mv = 512;
	mdl.affine_part.u0 = 512;
	mdl.affine_part.v0 = 512;

	return mdl;
}

void
test_pinhole(twirl_test::result *r, const void*)
{
	using twirl::vec2;
	using twirr::testdata::raydir_grid;

	auto const max_diff = 0.002f;

	auto const mdl0 = example_model();

	auto const dirs = raydir_grid<4>(1, 1);

	for (auto dir0: dirs) {
		auto const pt = twirl::pinhole::project(mdl0, dir0);
		auto const dir1 = twirl::pinhole::backproject(mdl0, pt);

		if (dir1.theta() > max_diff) {
			TWIRL_TEST_NEAREQ(r, dir0.phi(), dir1.phi(), max_diff);
		}
		TWIRL_TEST_NEAREQ(r, dir0.theta(), dir1.theta(), max_diff);
	}

}

int main()
{
	using twirl_test::xverdict;

	static const twirl_test::test_case tests[] = {
		{
			"Pinhole",
			xverdict::pass,
			test_pinhole,
			nullptr
		}
	};
	unsigned int const tests_n = sizeof tests / sizeof *tests;

	return twirl_test::run_tests(tests, tests_n);
}
