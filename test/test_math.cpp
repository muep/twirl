/*
Copyright (c) 2014 Joonas Sarajärvi

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/
#include <twirl/math.hpp>

#include "twirl_test.hpp"

static
void
test_sinish(twirl_test::result *r,
            const void *)
{
	twirl::math::sinish sinish;

	twirl::real_t const range_start = - twirl::math::pi * 4;
	twirl::real_t const range_end = twirl::math::pi * 4;
	twirl::real_t const range_len = range_end - range_start;

	unsigned int const steps = 1300;
	auto const interval = range_len / steps;

	auto const max_diff = static_cast<twirl::real_t>(0.008f);

	for (unsigned int n = 0; n < steps; ++n) {
		auto const x = range_start + interval * n;

		auto const target_cosx = twirl::math::cosr(x);
		auto const target_sinx = twirl::math::sinr(x);

		auto const approx_cosx = sinish.cos(x);
		auto const approx_sinx = sinish.sin(x);

		TWIRL_TEST_NEAREQ(r, target_cosx, approx_cosx, max_diff);
		TWIRL_TEST_NEAREQ(r, target_sinx, approx_sinx, max_diff);
	}
}

int main()
{
	using twirl_test::xverdict;

	static const twirl_test::test_case tests[] = {
		{
			"sinish",
			xverdict::pass,
			test_sinish,
			nullptr
		}	};
	unsigned int const tests_n = sizeof tests / sizeof *tests;

	return twirl_test::run_tests(tests, tests_n);
}
