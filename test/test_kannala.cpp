/*
Copyright (c) 2014 Joonas Sarajärvi

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/
#include <string.h>

#include <twirl/array.hpp>
#include <twirl/kannala.hpp>
#include <twirl/math.hpp>
#include <twirl/polynomy.hpp>

#include <twirr/testdata.hpp>

#include "twirl_test.hpp"

void
test_modelsize(twirl_test::result *r, const void *)
{
	using twirl::real_t;
	using twirl::kannala::full_model;

	auto const rt_size = sizeof (real_t);
	auto const fullmodel_size = sizeof (full_model);
	auto const num_count = fullmodel_size / rt_size;

	/* The paper talks about 23 parameters, but maybe it does
	 * not count the max theta in them */
	TWIRL_TEST_EQUAL_INT(r, num_count, 24);
}

void
test_raydir_of_v3(twirl_test::result * const r, const void *)
{
	using twirl::ray_direction;
	using twirl::real_t;
	using twirl::vec3;
	using twirl::kannala::raydir_of_v3;
	using twirl::math::pi;

	real_t const max_diff = 0.001f;

	/* Point on the principal axis */
	vec3 const pt0 = { 0.0f, 0.0f, -1.0f };
	ray_direction const rd0 = raydir_of_v3(pt0);
	TWIRL_TEST_NEAREQ(r, rd0.theta(), 0.0f, max_diff);

	/* Point that is offset to the right */
	vec3 const pt1 = { 1.0f, 0.0f, -1.0f };
	ray_direction const rd1 = raydir_of_v3(pt1);
	TWIRL_TEST_NEAREQ(r, rd1.theta(), pi / 4, max_diff);
	TWIRL_TEST_NEAREQ(r, rd1.phi(), 0, max_diff);

	/* Point that is offset upwards */
	vec3 const pt2 = { 0.0f, 1.0f, -1.0f };
	ray_direction const rd2 = raydir_of_v3(pt2);
	TWIRL_TEST_NEAREQ(r, rd2.theta(), pi / 4, max_diff);
	TWIRL_TEST_NEAREQ(r, rd2.phi(), pi / 2, max_diff);
}

/**
 * Do the symmetric projection for a couple of arbitrarily preselected
 * points, and see if symmetric backprojection from the projected points
 * produces the originals back. This alone does not really verify either
 * of the symmetric projection functions to be correct, but it helps to
 * see if they at least roughly reverse each other.
 */
void
test_symmetric_backforth(twirl_test::result *r, const void *)
{
	using twirl::ray_direction;
	using twirl::real_t;
	using twirl::vec2;
	using twirl::kannala::backproject_symmetric;
	using twirl::kannala::project_symmetric;
	using twirl::math::pi;

	using twirr::testdata::minimal_kannala_model;
	using twirr::testdata::raydir_grid;

	real_t const max_diff = 0.001f;
	twirl::polynomy const p;
	auto const model = minimal_kannala_model();
	auto const smodel = &(model.symmetric_part);

	auto const dirs = raydir_grid<4>(1,1);

	for (auto dir0: dirs) {
		auto const dir0_plus =
		static_cast<twirl::kannala::raydir_plus>(dir0);

		auto const pos0 = project_symmetric(p, *smodel, dir0_plus);
		auto const dir1 = backproject_symmetric(p, *smodel, pos0);

		TWIRL_TEST_NEAREQ(r, dir0.phi(), dir1.phi(), max_diff);
		TWIRL_TEST_NEAREQ(r, dir0.theta(), dir1.theta(), max_diff);
	}
}

void
test_distortion(twirl_test::result *, const void *)
{
	/* This does not really test anything yet, due to
	 * it not being clear what to expect from
	 * this. */
	using twirl::ray_direction;
	using twirl::real_t;
	using twirl::vec2;
	using twirl::kannala::asym_distortion;
	using twirl::kannala::distortion;
	using twirl::math::pi;

	twirl::polynomy const p;

	asym_distortion const tan_d = {
		{ 1.0f, 0.0f, 0.0f },
		{ 0.1f, 0.0f, 0.0f, 0.0f }
	};

	asym_distortion const rad_d = {
		{ 0.0f, 0.0f, 0.0f },
		{ 0.1f, 0.0f, 0.0f, 0.0f }
	};

	ray_direction const rd0 = { pi / 6, 0 };
	vec2 const d0 = distortion(
		p,
		rad_d,
		tan_d,
		static_cast<twirl::kannala::raydir_plus2>(rd0)
	);
	(void) d0;
}

void
test_fullmodel_backforth(twirl_test::result *r, const void *)
{
	using twirl::ray_direction;
	using twirl::real_t;
	using twirl::kannala::backproject;
	using twirl::kannala::project;
	using twirl::math::pi;

	using twirr::testdata::minimal_kannala_model;

	real_t const max_diff = 0.001f;
	auto const model = minimal_kannala_model();

	twirl::polynomy const p;

	ray_direction const rd00 = { 0.1f, pi / 4 };
	auto const pos0 = project(
		p,
		model,
		static_cast<twirl::kannala::raydir_plus2>(rd00)
	);
	ray_direction const rd01 = backproject(p, model, pos0);

	TWIRL_TEST_NEAREQ(r, rd00.phi(), rd01.phi(), max_diff);
	TWIRL_TEST_NEAREQ(r, rd00.theta(), rd01.theta(), max_diff);
}

void
test_raydir_conversions(twirl_test::result *r, const void *)
{
	using twirl::ray_direction;
	using twirl::real_t;
	using twirl::kannala::raydir_plus;
	using twirl::kannala::raydir_plus2;
	using twirl::math::cosr;
	using twirl::math::pi;
	using twirl::math::sinr;

	real_t const max_diff = 0.001f;

	/* Just a crude sampling from a PRNG */
	real_t const ph = 1.9986f;
	real_t const th = 2.2985f;

	/* Correct answers */
	real_t const cos_ph = cosr(ph);
	real_t const sin_ph = sinr(ph);

	real_t const cos_2ph = cosr(2 * ph);
	real_t const sin_2ph = sinr(2 * ph);

	/* The sole way to the the plain ray direction. */
	ray_direction const rd = { th, ph };
	TWIRL_TEST_NEAREQ(r, th, rd.theta(), max_diff);
	TWIRL_TEST_NEAREQ(r, ph, rd.phi(), max_diff);

	/* Two ways to get raydir_plus */
	raydir_plus const rdps[] = {
		raydir_plus(th, ph),
		raydir_plus(rd)
	};

	for (auto & rdp: rdps) {
		TWIRL_TEST_NEAREQ(r, th, rdp.theta(), max_diff);
		TWIRL_TEST_NEAREQ(r, ph, rdp.phi(), max_diff);
		TWIRL_TEST_NEAREQ(r, cos_ph, rdp.cos_phi(), max_diff);
		TWIRL_TEST_NEAREQ(r, sin_ph, rdp.sin_phi(), max_diff);
	}

	/* Even more ways to get the plus2 variant */
	raydir_plus2 const rdpps[] = {
		raydir_plus2(th, ph),
		raydir_plus2(rd),
		raydir_plus2(rdps[0]),
		raydir_plus2(rdps[1])
	};

	for (auto & rdpp: rdpps) {
		TWIRL_TEST_NEAREQ(r, th, rdpp.theta(), max_diff);
		TWIRL_TEST_NEAREQ(r, ph, rdpp.phi(), max_diff);
		TWIRL_TEST_NEAREQ(r, cos_ph, rdpp.cos_phi(), max_diff);
		TWIRL_TEST_NEAREQ(r, sin_ph, rdpp.sin_phi(), max_diff);
		TWIRL_TEST_NEAREQ(r, cos_2ph, rdpp.cos_2phi(), max_diff);
		TWIRL_TEST_NEAREQ(r, sin_2ph, rdpp.sin_2phi(), max_diff);
	}
}

int main()
{
	using twirl_test::xverdict;

	static const twirl_test::test_case tests[] = {
		{
			"Dummy test for reporting size of full model",
			xverdict::pass,
			test_modelsize,
			nullptr
		},
		{
			"Conversion from 3D coordinates to ray direction",
			xverdict::pass,
			test_raydir_of_v3,
			nullptr
		},
		{
			"Simple symmetric projection to both directions",
			xverdict::pass,
			test_symmetric_backforth,
			nullptr
		},
		{
			"Try out the distortion computation function",
			xverdict::pass,
			test_distortion,
			nullptr
		},
		{
			"Full projection to both directions",
			xverdict::pass,
			test_fullmodel_backforth,
			nullptr
		},
		{
			"ray_direction augmentations",
			xverdict::pass,
			test_raydir_conversions,
			nullptr
		}
	};
	unsigned int const tests_n = sizeof tests / sizeof *tests;

	return twirl_test::run_tests(tests, tests_n);
}
