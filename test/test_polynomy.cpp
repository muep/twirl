/*
Copyright (c) 2014 Joonas Sarajärvi

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/
#include <twirl/polynomy.hpp>

#include "twirl_test.hpp"

static
void
test_120(twirl_test::result *r,
         const void *)
{
	using twirl::real_t;

	const twirl::polynomy p;

	/*     polynomial:  x^2 + 2x                 *
	 * 1st derivative:  2x + 2                   *
	 * 2nd derivative:  2                        *
	 *                                           *
	 *                   order 2 coefficient     *
	 *                                   |       *
	 *            order 0 coeffiecient   |       *
	 *                             |     |       *
	 *                             V     V       */
	static const real_t k[] = { 0, 2, 1 };
	static const int order = (sizeof k / sizeof *k) - 1;

	const real_t poly_at_0 = p.derivative(k, order, 0, 0);
	const real_t poly_at_n3 = p.derivative(k, order, 0, -3);

	TWIRL_TEST_NEAREQ(r, poly_at_0, 0, 0.0001);
	TWIRL_TEST_NEAREQ(r, poly_at_n3, 3, 0.0001);

	const real_t deriv1_at_0 = p.derivative(k, order, 1, 0);
	const real_t deriv1_at_2 = p.derivative(k, order, 1, 2);
	TWIRL_TEST_NEAREQ(r, deriv1_at_0, 2, 0.0001);
	TWIRL_TEST_NEAREQ(r, deriv1_at_2, 6, 0.0001);

	const real_t deriv2_at_n2 = p.derivative(k, order, 2, -2);
	const real_t deriv2_at_0  = p.derivative(k, order, 2, 0);
	const real_t deriv2_at_2  = p.derivative(k, order, 2, -2);

	TWIRL_TEST_NEAREQ(r, deriv2_at_n2, 2, 0.0001);
	TWIRL_TEST_NEAREQ(r, deriv2_at_0, 2, 0.0001);
	TWIRL_TEST_NEAREQ(r, deriv2_at_2, 2, 0.0001);
}

struct root_test_answer {
	const twirl::real_t *polynomial;
	int polynomial_order;
	twirl::real_t expected_root;
};

static
void test_root(twirl_test::result *r,
               const void * v)
{
	const twirl::polynomy p;

	auto const param = static_cast<const root_test_answer*>(v);
	auto const root_value = p.root(param->polynomial,
	                               param->polynomial_order,
	                               0.001f);
	TWIRL_TEST_NEAREQ(r, root_value, param->expected_root, 0.01);
}

static
void test_135(twirl_test::result *r,
              const void *)
{
	const twirl::polynomy p;

	twirl::real_t const full[] = {
		0.0f,
		0.96432f,
		0.0f,
		0.17006f,
		0.0f,
		0.23088f
	};

	twirl::real_t const compact[] = {
		full[1],
		full[3],
		full[5]
	};

	twirl::real_t const max_diff = 0.0000001f;

	for (twirl::real_t x = 0; x < 1; x += 0.1f) {
		auto const y0 = p.value(full, 5, x);
		auto const y1 = p.value_135(compact, x);

		TWIRL_TEST_NEAREQ(r, y0, y1, max_diff);
	}
}

static
void test_13579(twirl_test::result *r,
                const void *)
{
	const twirl::polynomy p;

	twirl::real_t const full[] = {
		0.0f,
		0.54533f,
		0.0f,
		0.84231f,
		0.0f,
		0.65950f,
		0.0f,
		0.34362f,
		0.0f,
		0.59853f
	};

	twirl::real_t const compact[] = {
		full[1],
		full[3],
		full[5],
		full[7],
		full[9]
	};

	twirl::real_t const max_diff = 0.0000001f;

	for (twirl::real_t x = 0; x < 1; x += 0.1f) {
		auto const y0 = p.value(full, 9, x);
		auto const y1 = p.value_13579(compact, x);

		TWIRL_TEST_NEAREQ(r, y0, y1, max_diff);
	}
}


const twirl::real_t polynomial_01[] = { 0, 1 };
const twirl::real_t polynomial_n2101[] = { -2, 1, 0, 1 };

const root_test_answer root_answer_01 = {
	polynomial_01,
	1,
	0
};

const root_test_answer root_answer_n2101 = {
	polynomial_n2101,
	3,
	1
};

int main()
{
	using twirl_test::xverdict;

	static const twirl_test::test_case tests[] = {
		{
			"some values from x^2 + 2x",
			xverdict::pass,
			test_120,
			nullptr
		},
		{
			"find root of x",
			xverdict::pass,
			test_root,
			reinterpret_cast<const void*>(&root_answer_01)
		},
		{
			"find root of -2 + x + x^3",
			xverdict::pass,
			test_root,
			reinterpret_cast<const void*>(&root_answer_n2101)
		},
		{
			"value_135 special case function",
			xverdict::pass,
			test_135,
			nullptr
		},
		{
			"value_13579 special case function",
			xverdict::pass,
			test_13579,
			nullptr
		}
	};
	unsigned int const tests_n = sizeof tests / sizeof *tests;

	return twirl_test::run_tests(tests, tests_n);
}
