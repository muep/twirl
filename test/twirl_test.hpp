/*
Copyright (c) 2014 Joonas Sarajärvi

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/
/*
 * twirl_test.hpp
 *
 * A crude testing "framework" for easing the writing of
 * automated tests for twirl. */

#ifndef TWIRL_TEST_HPP
#define TWIRL_TEST_HPP

#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

#include "twirl.hpp"

namespace twirl_test {

enum class xverdict {
	pass,
	fail
};

struct result;

void
log_equal_int_fail(result *r,
                   const char * file,
                   int line,
                   const char *expr0,
                   int val0,
                   const char *expr1,
                   int val1);

void
log_neareq_fail(result *r,
                const char * file,
                int line,
                const char * expr0,
                twirl::real_t val0,
                const char * expr1,
                twirl::real_t val1);

void
log_require_fail(result *r,
                 const char *file,
                 int line,
                 const char *expr);

typedef void (*test_f)(result *result,
                       const void *data);

struct test_case {
	const char * title;
	xverdict expected;
	test_f function;
	const void *data;
};

int
run_tests(const test_case *tests,
          unsigned int n_tests);

}

/** Some helper macros */
#define _TWIRL_TEST_PREHASH(foo) #foo
#define _TWIRL_TEST_STR(foo) _TWIRL_TEST_PREHASH(foo)

#define _TWIRL_MSG_BUF_SIZE 80

#define TWIRL_TEST_EQUAL_INT(__r__,__val0__,__val1__)\
{\
	if (!((__val0__) == (__val1__))){\
		twirl_test::log_equal_int_fail(__r__,\
		                               __FILE__,\
									   __LINE__,\
									   _TWIRL_TEST_STR(__val0__),\
									   (__val0__),\
									   _TWIRL_TEST_STR(__val1__),\
									   (__val1__));\
	}\
}

#define TWIRL_TEST_NEAREQ(__r__,__val0__,__val1__,__s__)\
{\
	const auto dif = ((__val0__) - (__val1__));\
	const auto _a = dif >= 0 ? dif : -dif;\
	if (_a > (__s__)) {\
		twirl_test::log_neareq_fail(__r__,\
		                            __FILE__,\
		                            __LINE__,\
		                            _TWIRL_TEST_STR(__val0__),\
		                            (__val0__),\
		                            _TWIRL_TEST_STR(__val1__),\
		                            (__val1__));\
	}\
}

#define TWIRL_TEST_REQUIRE(r,expr)\
{\
	if (!(expr)) {\
		twirl_test::log_require_fail(r,\
		                             __FILE__,\
		                             __LINE__,\
		                             _TWIRL_TEST_STR(expr));\
	}\
}

#endif
