/*
Copyright (c) 2014 Joonas Sarajärvi

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/
#include <string.h>

#include <twirl/array.hpp>
#include <twirl/affine.hpp>
#include <twirl/math.hpp>

#include "twirl_test.hpp"

twirl::affine::model
example_model(void)
{
	twirl::affine::model mdl;

	/* Something like a 1024 x 1024 pixel sensor */
	mdl.mu = 512;
	mdl.mv = 512;
	mdl.u0 = 512;
	mdl.v0 = 512;

	return mdl;
}

void
test_affine(twirl_test::result *r, const void*)
{
	using twirl::affine::backward;
	using twirl::affine::forward;
	using twirl::vec2;

	auto const max_diff = 0.5f;

	auto const mdl0 = example_model();
	vec2 const pi0 = {
		120.6f,
		10.2f
	};
	auto px0 = forward(mdl0, pi0);
	auto po0 = backward(mdl0, px0);
	TWIRL_TEST_NEAREQ(r, pi0.x, po0.x, max_diff);
	TWIRL_TEST_NEAREQ(r, pi0.y, po0.y, max_diff);
	/* TODO: Here it might make sense to also check
	 * that the pixel positions are sane. */

	auto const max_diff2 = 0.5f / 256;

	/* A bit less trivial model. */
	twirl::affine::model const mdl1 = {
		/* mu */
		256,
		/* mv */
		256,
		/* u0 */
		256,
		/* v0 */
		192
	};

	/* Center */
	vec2 const center_in = {
		0.0,
		0.0
	};
	auto center_px = forward(mdl1, center_in);
	auto center_out = backward(mdl1, center_px);
	TWIRL_TEST_NEAREQ(r, center_in.x, center_out.x, max_diff2);
	TWIRL_TEST_NEAREQ(r, center_in.y, center_out.y, max_diff2);

	vec2 const topleft_in = {
		-1.0f,
		1.0f
	};

	auto const topleft_px = forward(mdl1, topleft_in);
	auto const topleft_out = backward(mdl1, topleft_px);
	TWIRL_TEST_NEAREQ(r, topleft_in.x, topleft_out.x, max_diff2);
	TWIRL_TEST_NEAREQ(r, topleft_in.y, topleft_out.y, max_diff2);
}

int main()
{
	using twirl_test::xverdict;

	static const twirl_test::test_case tests[] = {
		{
			"Affine transform",
			xverdict::pass,
			test_affine,
			nullptr
		}
	};
	unsigned int const tests_n = sizeof tests / sizeof *tests;

	return twirl_test::run_tests(tests, tests_n);
}
