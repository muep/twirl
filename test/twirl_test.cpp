/*
Copyright (c) 2014 Joonas Sarajärvi

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/
#include "twirl_test.hpp"

#include <iostream>
#include <string>
#include <sstream>
#include <vector>

struct twirl_test::result {
	twirl_test::test_case const *test_case;
	std::vector<std::string> problems;
};

void
twirl_test::log_equal_int_fail(result *r,
                               const char * file,
                               int line,
                               const char *expr0,
                               int val0,
                               const char *expr1,
                               int val1)
{
	std::stringstream s;
	s << "at " << file << ":" << line << std::endl;
	s << "    expected " << val0 << " (from " << expr0 << ")" << std::endl;
	s << "to be nearly " << val1 << " (from " << expr1 << ")" << std::endl;
	r->problems.push_back(s.str());
}

void
twirl_test::log_neareq_fail(result *r,
                            const char * file,
                            int line,
                            const char * expr0,
                            twirl::real_t val0,
                            const char * expr1,
                            twirl::real_t val1)
{
	std::stringstream s;
	s << "at " << file << ":" << line << std::endl;
	s << "    expected " << val0 << " (from " << expr0 << ")" << std::endl;
	s << "to be nearly " << val1 << " (from " << expr1 << ")" << std::endl;
	s << " (error = " << (val1 - val0) << ")" << std::endl;
	r->problems.push_back(s.str());
}

void
twirl_test::log_require_fail(result * const r,
                             const char * const file,
                             int const line,
                             const char * const expr)
{
	std::stringstream s;
	s << "at " << file << ":" << line << std::endl;
	s << "  expected " << expr << std::endl;
	r->problems.push_back(s.str());
}

int
twirl_test::run_tests(const twirl_test::test_case *tests,
                      unsigned int const n_tests)
{
	size_t problem_count = 0;

	for (unsigned int n = 0; n < n_tests; ++n) {
		twirl_test::result r;
		r.test_case = tests + n;
		std::cout << "case " << n << ": " << r.test_case->title
		          << ":" << std::endl;

		/* Run the actual test code here */
		r.test_case->function(&r, r.test_case->data);

		auto const case_problem_count = r.problems.size();
		if (r.test_case->expected == xverdict::pass) {
			problem_count += case_problem_count;
		}

		if (case_problem_count == 0) {
			std::cout << " PASS" << std::endl;
		} else {
			if (r.test_case->expected == xverdict::fail) {
				std::cout << " XFAIL" << std::endl;
			} else {
				std::cout << " FAIL" << std::endl;
			}
		}

		for (auto problem: r.problems) {
			if (problem.length() > 0) {
				std::cout << " " << problem << std::endl;
			}
		}
	}

	return problem_count == 0 ? 0 : 1;
}
