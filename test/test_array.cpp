/*
Copyright (c) 2014 Joonas Sarajärvi

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/
#include <twirl/array.hpp>

#include "twirl_test.hpp"

static
void
test_slice(twirl_test::result *r,
           const void *)
{
	using twirl::array;

	int tmp = 0;
	twirl::array<int, 4> ints;
	for (auto &n: ints) {
		n = tmp++;
	}

	TWIRL_TEST_EQUAL_INT(r, ints.size(), 4);
	auto fst = ints.slice<2, 1>();
	TWIRL_TEST_EQUAL_INT(r, fst.size(), 1);
	TWIRL_TEST_EQUAL_INT(r, fst[0], ints[2]);
}

int main()
{
	using twirl_test::xverdict;

	static const twirl_test::test_case tests[] = {
		{
			"slicing",
			xverdict::pass,
			test_slice,
			nullptr
		}
	};
	unsigned int const tests_n = sizeof tests / sizeof *tests;

	return twirl_test::run_tests(tests, tests_n);
}
