/*
Copyright (c) 2014 Joonas Sarajärvi

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/
#include <twirl/matrix.hpp>

#include "twirl_test.hpp"

static
void
test_identity(twirl_test::result *r,
              const void *)
{
	using twirl::matrix;
	using twirl::real_t;

	real_t const max_diff = 0.001f;

	auto const i = matrix<2,2>::identity();

	TWIRL_TEST_NEAREQ(r, i[0][0], 1, max_diff);
	TWIRL_TEST_NEAREQ(r, i[1][0], 0, max_diff);
	TWIRL_TEST_NEAREQ(r, i[0][1], 0, max_diff);
	TWIRL_TEST_NEAREQ(r, i[1][1], 1, max_diff);
}

static
void
test_scalar_mul(twirl_test::result *r,
                const void *)
{
	using twirl::matrix;
	using twirl::real_t;

	real_t const max_diff = 0.001f;

	real_t const m0_v[] = { 1, 1, 1, 1 };

	matrix<2,2> const m0(m0_v);
	auto const m1 = 2 * m0;

	TWIRL_TEST_NEAREQ(r, m1[0][0], 2, max_diff);
	TWIRL_TEST_NEAREQ(r, m1[1][0], 2, max_diff);
	TWIRL_TEST_NEAREQ(r, m1[0][1], 2, max_diff);
	TWIRL_TEST_NEAREQ(r, m1[1][1], 2, max_diff);
}

static
void
test_mul_2x2_2x1(twirl_test::result *r,
                 const void *)
{
	using twirl::matrix;
	using twirl::real_t;

	real_t const max_diff = 0.001f;

	real_t const a_values[] = { 1, 2, 3, -4 };
	matrix<2,2> const a(a_values);;

	TWIRL_TEST_NEAREQ(r, a[0][0], 1, max_diff);
	TWIRL_TEST_NEAREQ(r, a[1][0], 3, max_diff);

	real_t const b0_values[] = { 0, 0 };
	matrix<2,1> const b0(b0_values);

	auto a_b0 = a * b0;
	TWIRL_TEST_EQUAL_INT(r, 2, a_b0.height());
	TWIRL_TEST_EQUAL_INT(r, 1, a_b0.width());

	for (int j = 0; j < 2; ++j) {
		TWIRL_TEST_NEAREQ(r, a_b0[j][0], 0, max_diff);
	}

	real_t const b1_values[] = { 1, 1, };
	matrix<2,1> const b1(b1_values);

	auto a_b1 = a * b1;
	TWIRL_TEST_NEAREQ(r, a_b1[0][0], 3, max_diff);
	TWIRL_TEST_NEAREQ(r, a_b1[1][0], -1, max_diff);
}

static
void
test_mul_2x3_3x2(twirl_test::result *r,
                 const void *)
{
	using twirl::matrix;
	using twirl::real_t;

	real_t const max_diff = 0.001f;

	real_t const v0[] = { 1.0f, 2.0f, 3.0f,
	                      4.0f, 5.0f, 6.0f };
	real_t const v1[] = { 1.0f, 2.0f,
	                      3.0f, 4.0f,
	                      5.0f, 6.0f };

	matrix<2,3> const m0(v0);
	matrix<3,2> const m1(v1);

	auto const m2 = m0 * m1;
	TWIRL_TEST_EQUAL_INT(r, m2.height(), 2);
	TWIRL_TEST_EQUAL_INT(r, m2.width(), 2);

	TWIRL_TEST_NEAREQ(r, m2[0][0], 22, max_diff);
	TWIRL_TEST_NEAREQ(r, m2[0][1], 28, max_diff);
	TWIRL_TEST_NEAREQ(r, m2[1][0], 49, max_diff);
	TWIRL_TEST_NEAREQ(r, m2[1][1], 64, max_diff);
}

static
void
test_mat2x2_inverse(twirl_test::result *r,
                    const void *)
{
	using twirl::matrix;
	using twirl::matrix2x2::invert;
	using twirl::real_t;

	real_t const max_diff = 0.001f;
	auto const identity = matrix<2,2>::identity();

	real_t const orig_v[] = { 1, 2, 3, 4 };
	matrix<2,2> const orig(orig_v);

	auto const inversed = invert(orig);

	auto const orig_by_inv = orig * inversed;
	auto const inv_by_orig = inversed * orig;

	/* All of these should be close to being an identity matrix */
	matrix<2,2> const * const identities[] = {
		&orig_by_inv,
		&inv_by_orig
	};

	for (auto mp: identities) {
		const matrix<2,2> &m = *mp;

		TWIRL_TEST_NEAREQ(r, m[0][0], identity[0][0], max_diff);
		TWIRL_TEST_NEAREQ(r, m[0][1], identity[0][1], max_diff);
		TWIRL_TEST_NEAREQ(r, m[1][0], identity[1][0], max_diff);
		TWIRL_TEST_NEAREQ(r, m[1][1], identity[1][1], max_diff);
	}
}

static
void
test_ones(twirl_test::result *r,
          const void *)
{
	using twirl::matrix;
	using twirl::real_t;

	real_t const max_diff = 0.001f;

	auto const m0 = matrix<2,2>::ones();
	auto const m1 = matrix<2,1>::ones();

	auto const m2 = m0 * m1;
	TWIRL_TEST_EQUAL_INT(r, m2.height(), 2);
	TWIRL_TEST_EQUAL_INT(r, m2.width(), 1);

	TWIRL_TEST_NEAREQ(r, m0[0][0], 1, max_diff);
	TWIRL_TEST_NEAREQ(r, m0[1][0], 1, max_diff);
	TWIRL_TEST_NEAREQ(r, m0[0][1], 1, max_diff);
	TWIRL_TEST_NEAREQ(r, m0[1][1], 1, max_diff);
	TWIRL_TEST_NEAREQ(r, m1[0][0], 1, max_diff);
	TWIRL_TEST_NEAREQ(r, m1[1][0], 1, max_diff);
	/* These values are not directly from ::ones, so
	 * we do not necessarily expect 1 */
	TWIRL_TEST_NEAREQ(r, m2[0][0], 2, max_diff);
	TWIRL_TEST_NEAREQ(r, m2[0][1], 2, max_diff);
}


int main()
{
	using twirl_test::xverdict;

	static const twirl_test::test_case tests[] = {
		{
			"identity matrix",
			xverdict::pass,
			test_identity,
			nullptr
		},
		{
			"matrix::ones()",
			xverdict::pass,
			test_ones,
			nullptr
		},
		{
			"scalar multiplication",
			xverdict::pass,
			test_scalar_mul,
			nullptr
		},
		{
			"mul_2x2_2x1",
			xverdict::pass,
			test_mul_2x2_2x1,
			nullptr
		},
		{
			"mul_2x3_3x2",
			xverdict::pass,
			test_mul_2x3_3x2,
			nullptr
		},
		{
			"inverse of matrix<2,2>",
			xverdict::pass,
			test_mat2x2_inverse,
			nullptr
		}
	};
	unsigned int const tests_n = sizeof tests / sizeof *tests;

	return twirl_test::run_tests(tests, tests_n);
}
