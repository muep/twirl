/*
Copyright (c) 2014 Joonas Sarajärvi

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/
/*
 * kannala.hpp
 *
 * Functionality for modeling the camera model that
 * is described in
 *
 * http://www.ee.oulu.fi/~jkannala/calibration/Kannala_Brandt_calibration.pdf
 */

#ifndef TWIRL_KANNALA_HPP
#define TWIRL_KANNALA_HPP

#include "twirl.hpp"

#include <stdint.h>

#include "affine.hpp"
#include "math.hpp"

namespace twirl {

struct polynomy;

namespace kannala {

/**
 * Basic symmetric camera model.
 */
struct symmetric_model {
	/*
	 * Values from k1 to k5 as mentioned in equation 6.
	 *
	 * The polynomial for radius is expected to be computable
	 * from these factors like this:
	 *
	 * r = k[0] * t + k[1] * t^3 + k[2] * t^5 + k[3] * t^7 + k[4] * t^9
	 */
	real_t k[5];

	/* Maximum angle between lens axis and incoming light ray
	 * direction that is assumed to get projected to light sensor. */
	real_t th_max;
};

/**
 * Asymmetric distortion model.
 *
 * The asymmetric distortions are modeled as a product
 *
 *  d_theta * d_phi
 *
 * where d_theta describes how strong the distortion at areas
 * far from the centerline, while d_phi describes the dependency
 * between distortion amount and the angle around the centerline.
 *
 * The expressions for these are further discussed next to their
 * respective model properties.
 */
struct asym_distortion {
	/**
	 * Relationship between theta and distortion amount.
	 *
	 * d_th[0] * th + d_th[1] * th^3 + d_th[2] * th^5
	 */
	real_t d_th[3];

	/**
	 * Relationship between phi and distortion amount.
	 */
	real_t d_ph[4];
};

struct full_model {
	symmetric_model symmetric_part;
	asym_distortion radial_distortion;
	asym_distortion tangential_distortion;
	affine::model affine_part;
};

/**
 * An extension of ray_direction that adds precomputed
 * sin and cos for phi into the structure.
 *
 * Just as an observation, this seems to be one of the
 * cases where slicing on conversion to base class is
 * exactly the useful thing to do.
 */
struct raydir_plus : public ray_direction {
	/**
	 * Upgrade from twirl::ray_direction.
	 */
	explicit raydir_plus(ray_direction const dir)
	  : raydir_plus(dir.theta(), dir.phi())
	{}

	raydir_plus(real_t const th, real_t const ph)
	  : ray_direction(th, ph),
	    m_cos_phi(math::cosr(ph)),
	    m_sin_phi(math::sinr(ph))
	{}

	real_t
	cos_phi() const
	{
		return m_cos_phi;
	}

	real_t
	sin_phi() const
	{
		return m_sin_phi;
	}

	vec2
	phi_rotated(vec2 const v) const
	{
		real_t const x = m_cos_phi * v.x - m_sin_phi * v.y;
		real_t const y = m_sin_phi * v.x + m_cos_phi * v.y;

		return vec2 { x, y };
	}

private:
	real_t m_cos_phi;
	real_t m_sin_phi;
};

/**
 * Even more cached stuff on top of ray_direction */
struct raydir_plus2 : public raydir_plus {
	/**
	 * Upgrade from plain ray_direction
	 */
	explicit raydir_plus2(ray_direction const dir)
	  : raydir_plus2(dir.theta(), dir.phi())
	{}

	/**
	 * Upgrade from plus to plus2.
	 */
	explicit raydir_plus2(raydir_plus const dir)
	  : raydir_plus(dir),
	    m_cos_2phi(math::cosr(2 * dir.phi())),
	    m_sin_2phi(math::sinr(2 * dir.phi()))
	{}

	/**
	 * Standalone constructor */
	raydir_plus2(real_t const th, real_t const ph)
	  : raydir_plus(th, ph),
	    m_cos_2phi(math::cosr(2 * ph)),
	    m_sin_2phi(math::sinr(2 * ph))
	{}

	real_t
	cos_2phi() const
	{
		return m_cos_2phi;
	}

	real_t
	sin_2phi() const
	{
		return m_sin_2phi;
	}

private:
	real_t m_cos_2phi;
	real_t m_sin_2phi;
};

/**
 * Load a full_model object from a chunk of bytes.
 *
 * The @param model_bytes must point to a byte array
 * whose length is 23 * 4 bytes.
 */
full_model
model_from_bytes(const uint8_t *model_bytes);

/**
 * Find out a reasonable max_theta for a model.
 *
 * The models output by the calibration toolbox
 * do not specify the maximum expected theta value
 * for the model. This could be computed by reverse
 * projecting the corners of an image, but the model
 * does not even seem to say anything about the image
 * size, despite that output from forward projection
 * is in pixels.
 *
 * This function takes the image size as its second
 * parameter, so it will hopefully produce a somewhat
 * accurate representation of the field of width,
 * provided that picture_size matches the size of images
 * that was used in the calibration tool.
 */
real_t
estimated_max_theta(const polynomy &p,
                    const full_model &model,
                    isize picture_size);

/**
 * Find out the ray direction from a 3D point that
 * is in camera aligned coordinate system.
 *
 * The coordinate system is oriented so that the
 * camera is looking towards the negative Z axis.
 *
 * Positive X is to the right of the camera, and
 * positive Y axis points upwards.
 */
ray_direction
raydir_of_v3(vec3 pt);

vec2
project_symmetric(const polynomy &p,
                  const symmetric_model &model,
                  const raydir_plus &dir);

vec2
distortion(const polynomy &p,
           const asym_distortion &radial,
           const asym_distortion &tangential,
           const raydir_plus2 &raydir);

/**
 * Approximate the effects of the distortion function.
 * from the distortion function.
 *
 * The asymmetric distortion part of the camera model is
 * defined as a function of the incoming light ray direction.
 * In case the direction is not known but the position in the
 * sensor plane is, this function can be used to estimate
 * the value of the asymmetric distortion for a light ray
 * direction that the full model placed at the point we know.
 *
 * When an estimate for the distortion is known,
 * the direction of the original light ray can be computed
 * just by subtracting the distortion from the point and
 * then using the radially symmetric backprojection.
 */
vec2
undistortion(const polynomy &p,
             const full_model &model,
             vec2 pos);

vec2
project(const polynomy &p,
        const full_model &model,
        const raydir_plus2 &dir);

ray_direction
backproject_symmetric(const polynomy &p,
                      const symmetric_model &model,
                      vec2 pos);

ray_direction
backproject(const polynomy &p,
            const full_model &model,
            vec2 pos);

}
}

#endif
