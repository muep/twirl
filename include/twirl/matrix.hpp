/*
Copyright (c) 2014 Joonas Sarajärvi

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/
#ifndef TWIRL_MATRIX_HPP
#define	TWIRL_MATRIX_HPP

#include "twirl.hpp"

#include <string.h>

namespace twirl {

template <int H, int W>
struct matrix {
	matrix()
	{}

	matrix(const real_t * const values)
	{
		size_t const amount = sizeof(real_t) * H * W;
		memcpy(a, values, amount);
	}

	const real_t *
	operator[](int n) const
	{
		return a[n];
	}

	int
	width() const
	{
		return W;
	}

	int height() const
	{
		return H;
	}

	template<int W2>
	matrix<H,W2>
	operator*(const matrix<W,W2> &o) const
	{
		/* H2 must be equal to W */
		matrix<H,W2> out;
		for (int j = 0; j < H; ++j) {
			for (int i2 = 0; i2 < W2; ++i2) {
				real_t &x = out.a[j][i2];
				x = 0;
				for (int i = 0; i < W; ++i) {
					x += a[j][i] * o.a[i][i2];
				}
			}
		}
		return out;
	}

	matrix
	operator*(real_t const v) const
	{
		matrix result = *this;

		for (int row = 0; row < H; ++row) {
			for (int col = 0; col < W; ++col) {
				result.a[row][col] *= v;
			}
		}

		return result;
	}

	matrix
	operator+(const matrix &o) const
	{
		matrix out;
		for (int j = 0; j < H; ++j) {
			for (int i = 0; i < W; ++i) {
				out.a[j][i] = a[j][i] + o.a[j][i];
			}
		}
		return out;
	}

	static
	matrix
	identity()
	{
		static_assert(H == W, "Width and height are not equal");

		matrix result;

		for (int row = 0; row < H; ++row) {
			for (int col = 0; col < W; ++col) {
				result.a[row][col] = row == col ? 1.0f : 0.0f;
			}
		}

		return result;
	}

	/**
	 * Get a matrix that is filled with ones.
	 */
	static
	matrix
	ones()
	{
		matrix result;

		for (int row = 0; row < H; ++row) {
			for (int col = 0; col < W; ++col) {
				result.a[row][col] = 1;
			}
		}

		return result;
	}

	real_t a[H][W];
};

template<int H, int W>
matrix<H,W> operator*(real_t const v, const matrix<H,W> &m)
{
	return m * v;
}

namespace matrix2x2 {

matrix<2,2>
invert(matrix<2,2> m);

} /* namespace matrix2x2 */
} /* namespace twirl */

#endif	/* TWIRL_MATRIX_HPP */
