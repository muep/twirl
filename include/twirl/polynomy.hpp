/*
Copyright (c) 2014 Joonas Sarajärvi

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/
#ifndef TWIRL_POLYNOMY_HPP
#define TWIRL_POLYNOMY_HPP

#include <twirl.hpp>

#include <stdint.h>

namespace twirl {

/**
 * Mechanism for computing values and derivatives
 * of polynomials.
 *
 * While all the functionality in the polynomy class could
 * be implemented as free functions, this implementation stores
 * some look-up tables and such things into itself.
 *
 * The creation of a polynomy object is fairly big when compared
 * to computing a single polynomial value. Thus it is highly
 * recommended that a same object is reused when possible.
 */
struct polynomy {
	polynomy();

	/**
	 * Compute value of a polynomial or one of its derivatives.
	 *
	 * The first two parameters specify the polynomial. The third
	 * parameter specifies for which of the polynomial's derivatives
	 * the resulting value should be. For computing values of the
	 * polynomial itself instead of its derivatives, pass zero here.
	 * The fourth parameter is the sole parameter to the polynomial
	 * function or one of its derivative polynomial functions. */
	real_t
	derivative(const real_t * coeffs,
	           uint_fast16_t polynomial_order,
	           uint_fast16_t derivative_order,
	           real_t x) const;

	/**
	 * Compute the root of a polynomial.
	 */
	real_t
	root(const real_t *polynomial,
	     uint_fast16_t polynomial_order,
	     real_t max_diff) const;

	/**
	 * Same as derivative, but with derivative_order
	 * set to 0. */
	real_t
	value(const real_t * coeffs,
	      uint_fast16_t polynomial_order,
	      real_t x) const;

	real_t
	value_13579(const real_t * coeffs,
	            real_t x) const;

	real_t
	value_135(const real_t * coeffs,
	          real_t x) const;

private:
	/* Some space for maintenance of internal lookup tables. */
	real_t d[48];
};

} /* namespace twirl */

#endif
