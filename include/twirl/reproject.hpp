/*
Copyright (c) 2014 Joonas Sarajärvi

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/
#ifndef TWIRL_REPROJECT_HPP
#define TWIRL_REPROJECT_HPP

#include <stdint.h>

namespace twirl {

namespace kannala {
struct full_model;
} /* namespace kannala */

namespace reproject {

/**
 * Directly correct part of an image.
 *
 * The number of parameters is getting pretty silly. For simple
 * cases, the whole function below may help readability a bit.
 */
void
partial(const kannala::full_model &fm,
        uint32_t width,
        uint32_t height,
        uint32_t black,
        const uint32_t * data_in,
        uint32_t * data_out,
        uint32_t from_row,
        uint32_t to_row);

/**
 * Directly correct an image.
 *
 * Same as calling twirl::reproject::partial with
 * 7th parameter set to 0 and 8th parameter set to
 * same as height.
 */
void
whole(const kannala::full_model &fm,
      uint32_t width,
      uint32_t height,
      uint32_t black,
      const uint32_t * data_in,
      uint32_t * data_out);

/**
 * Look-up table entry.
 */
struct lut_entry {
	uint32_t offset;
};

/**
 * A variant of the build_lut function that
 * builds just a part of the look-up table.
 */
void
build_partial_lut(const kannala::full_model &mdl,
                  uint32_t width,
                  uint32_t height,
                  lut_entry *target,
                  uint32_t from_row,
                  uint32_t to_row);

/**
 * Prebuild look-up table that can be used with
 * the reprojection functions.
 *
 * Same as calling build_partial_lut with 5th parameter
 * set to 0 and 6th parameter to same as height.
 */
void
build_lut(const kannala::full_model &mdl,
          uint32_t width,
          uint32_t height,
          lut_entry * target);

/**
 * Correct an image with a prebuilt look-up table.
 *
 * This is supposed to be faster than just using
 * the direct computation.
 */
void
correct_with_lut(const lut_entry *lut,
                 uint32_t from_pos,
                 uint32_t to_pos,
                 uint32_t black,
                 const uint32_t * data_in,
                 uint32_t * data_out);

} /* namespace reproject */
} /* namespace twirl */

#endif
