/*
Copyright (c) 2014 Joonas Sarajärvi

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/
/*
 * array.hpp
 *
 * Defines the twirl::array container template
 */

#ifndef TWIRL_ARRAY_HPP
#define TWIRL_ARRAY_HPP

namespace twirl {

/**
 * A general-purpose container template for storing a fixed
 * small number of something.
 */
template <typename T, int L>
struct array {
	typedef T * iterator;
	typedef T const * const_iterator;
	/**
	 * Constructor that relies on elements being
	 * default-constructed.
	 */
	array() {}

	/**
	 * Constructor that copies L elements from the parameter.
	 */
	array(T const * elems)
	{
		for (int n = 0; n < L; ++n) {
			data[n] = elems[n];
		}
	}

	/**
	 * Get the number of elements in the array. Sometimes useful,
	 * despite that this could be trivially gotten from the type
	 * of the variable.
	 */
	int size() const { return L; }

	/* These allow accessing elements as if the array
	 * object was a C array. */
	T & operator[](int pos) { return data[pos]; }
	T const & operator[](int pos) const { return data[pos]; }


	/* Implicit conversions to a pointer to the first element,
	 * similar to how it is done for C arrays */
	operator T*() { return data; }
	operator const T*() const { return data; }

	iterator begin() { return data; }
	const_iterator begin() const { return data; }

	iterator end() { return data + L; }
	const_iterator end() const { return data + L; }

	/**
	 * Get a subarray of the array.
	 *
	 * This is implemented as a template, in order to enforce
	 * bounds checking at compile time.
	 */
	template<int start, int L2>
	array<T,L2> slice()
	{
		static_assert(start + L2 < L, "slice out of bounds");

		return array<T,L2>(data + start);
	}
private:
	T data[L];
};

} /* namespace twirl */

#endif
