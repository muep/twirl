/*
Copyright (c) 2014 Joonas Sarajärvi

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/
#ifndef TWIRL_AFFINE_HPP
#define TWIRL_AFFINE_HPP

#include "twirl.hpp"

namespace twirl {
namespace affine {

/**
 * Structure for representing an affine transform from
 * normalized image coordinates to pixel coordinates.
 *
 * Originally written as part of the kannala model, but
 * nowadays separated to be easier usable from multiple
 * modules.
 */
struct model {
	/* Number of pixels per unit of movement in horizontal direction */
	real_t mu;
	/* Number of pixels per unit of movement in vertical direction */
	real_t mv;
	/* Principal point */
	real_t u0;
	real_t v0;
};

static inline
vec2
backward(const model &mdl,
         vec2 const px)
{
	vec2 p;
	p.x = (px.x - mdl.u0) / mdl.mu;
	p.y = (px.y - mdl.v0) / mdl.mv;
	return p;
}

static inline
vec2
forward(const model &mdl,
        vec2 const p)
{
	vec2 px;
	px.x = mdl.mu * p.x + mdl.u0;
	px.y = mdl.mv * p.y + mdl.v0;
	return px;
}

} /* namespace affine */
} /* namespace twirl */

#endif
