/*
Copyright (c) 2014 Joonas Sarajärvi

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/
#ifndef TWIRL_PINHOLE_HPP
#define TWIRL_PINHOLE_HPP

#include "twirl.hpp"

#include <stdint.h>

#include "affine.hpp"

namespace twirl {
namespace pinhole {

/**
 * Pinhole camera model.
 */
struct model {
	/* Focal length */
	real_t focal_length;

	affine::model affine_part;
};

ray_direction
backproject(const model &mdl, vec2 pos);

vec2
project(const model &mdl, ray_direction dir);

model
model_for_fov_and_size(real_t fov, uint16_t width, uint16_t height);


} /* namespace pinhole */
} /* namespace twirl */

#endif
