/*
Copyright (c) 2014 Joonas Sarajärvi

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/
#ifndef TWIRL_MATH_HPP
#define TWIRL_MATH_HPP

#include "twirl.hpp"

#include <math.h>

namespace twirl {
namespace math {

/*
 * The naming of things in math.h is a bit unfortunate.
 * so we here try to define some constant names for
 * things that operate on real_t.
 */

#ifdef TWIRL_REAL_IS_FLOAT
static const real_t pi = 3.14159f;

static inline real_t sinr(real_t const x) { return sinf(x); }
static inline real_t cosr(real_t const x) { return cosf(x); }
static inline real_t tanr(real_t const x) { return tanf(x); }
static inline real_t atanr(real_t const x) { return atanf(x); }
static inline real_t atan2r(real_t const y, real_t const x)
{
	return atan2f(y, x);
}
static inline real_t sqrtr(real_t const x) { return sqrtf(x); }
static inline real_t floorr(real_t const x) { return floorf(x); }
#endif

#ifdef TWIRL_REAL_IS_DOUBLE
static const real_t pi = 3.14159;

static inline real_t sinr(real_t const x) { return sin(x); }
static inline real_t cosr(real_t const x) { return cos(x); }
static inline real_t tanr(real_t const x) { return tan(x); }
static inline real_t atanr(real_t const x) { return atan(x); }
static inline real_t atan2r(real_t const y, real_t const x)
{
	return atan2(y, x);
}
static inline real_t sqrtr(real_t const x) { return sqrt(x); }
static inline real_t floorr(real_t const x) { return floor(x); }
#endif

struct sinish {
	sinish();

	real_t sin(real_t x) const;
	real_t cos(real_t y) const;

private:
	real_t quarter[256];
};

} /* namespace math */
} /* namespace twirl */

#endif
