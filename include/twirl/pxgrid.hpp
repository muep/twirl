/*
Copyright (c) 2014 Joonas Sarajärvi

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/
#ifndef TWIRL_PXGRID_HPP
#define TWIRL_PXGRID_HPP

#include "twirl.hpp"

namespace twirl
{
namespace pxgrid
{

static inline
ivec2
ivec2_rounded_from_vec2(vec2 const v)
{
	ivec2 v2;
	v2.x = static_cast<int> (v.x + 0.5f);
	v2.y = static_cast<int> (v.y + 0.5f);
	return v2;
}

static inline
vec2
vec2_from_ivec2(ivec2 const v)
{
	vec2 v2;
	v2.x = static_cast<twirl::real_t>(v.x);
	v2.y = static_cast<twirl::real_t>(v.y);
	return v2;
}

} /* namespace pxgrid */
} /* namespace twirl */

#endif
