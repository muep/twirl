/*
Copyright (c) 2014 Joonas Sarajärvi

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/
/*
 * vec2_ops.hpp
 *
 * A minimal set of arithmetic operator overloads for twirl::vec2.
 *
 * Namely, this headers has operators for these cases:
 * - addition and subtraction of vec2 objects with other vec2 objects
 * - multiplication of vec2 with a scalar
 */

#ifndef TWIRL_VEC2_OPS_HPP
#define TWIRL_VEC2_OPS_HPP

#include "twirl.hpp"

namespace twirl {

static inline
vec2 &
operator+=(vec2 &v, const vec2 &other)
{
	v.x += other.x;
	v.y += other.y;
	return v;
}

static inline
vec2 &
operator-=(vec2 &v, const vec2 &other)
{
	v.x -= other.x;
	v.y -= other.y;
	return v;
}

static inline
vec2 &
operator*=(vec2 &v, real_t a)
{
	v.x *= a;
	v.y *= a;
	return v;
}

static inline
vec2
operator-(vec2 v)
{
	v.x = -v.x;
	v.y = -v.y;
	return v;
}

static inline
vec2
operator+(vec2 v, const vec2 &other)
{
	v += other;
	return v;
}

static inline
vec2
operator-(vec2 v, const vec2 &other)
{
	v -= other;
	return v;
}

static inline
vec2
operator*(vec2 v, real_t a)
{
	v *= a;
	return v;
}

static inline
vec2
operator*(real_t a, vec2 v)
{
	v *= a;
	return v;
}

} /* namespace twirl */

#endif
