/*
Copyright (c) 2014 Joonas Sarajärvi

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/
#ifndef TWIRR_REPROJECT_HPP
#define TWIRR_REPROJECT_HPP

#include <stdint.h>

namespace twirl {

namespace kannala {
struct full_model;
} /* namespace kannala */

namespace reproject {
struct lut_entry;
} /* namespace reproject */

} /* namespace twirl */

namespace twirr {

/**
 * This namespace contains routines that run their equivalent
 * functions from twirl::reproject so that the work gets divided
 * between multiple threads.
 *
 * All of the functions currently work such that they allocate
 * the work to a number of threads, start the threads and then
 * wait for their completion. This is a bit crude, but it does
 * demonstrably help with performance.
 */
namespace parallel_reproject {

/**
 * Directly correct an image, using multiple threads. This should be
 * the same thing as using twirl::reproject::whole, but faster on
 * hardware that benefits from multi-threaded execution.
 */
void
parallel(const twirl::kannala::full_model &fm,
         uint32_t width,
         uint32_t height,
         uint32_t black,
         const uint32_t * data_in,
         uint32_t * data_out);

void
build_lut_parallel(const twirl::kannala::full_model &m,
                   uint32_t width,
                   uint32_t height,
                   twirl::reproject::lut_entry * target);

/*
 * Correct an image with a LUT, using multiple threads.
 *
 * In some cases, threads started by this function
 * have so little work to do that the whole division of
 * work to threads does not work very well.
 * It may not necessarily be able to saturate
 * the available CPU capacity.
 */
void
with_lut_parallel(const twirl::reproject::lut_entry *lut,
                  uint32_t size,
                  uint32_t black,
                  const uint32_t * image_in,
                  uint32_t * image_out);

} /* namespace reproject */
} /* namespace twirr */

#endif
