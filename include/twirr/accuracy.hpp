/*
Copyright (c) 2014 Joonas Sarajärvi

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/
#ifndef TWIRR_ACCURACY_HPP
#define	TWIRR_ACCURACY_HPP

#include <cstdint>
#include <vector>

#include <twirl/kannala.hpp>

namespace twirr {
namespace accuracy {

struct result_t {
	twirl::kannala::full_model model;
	std::uint16_t width;
	std::uint16_t height;
	std::vector<twirl::real_t> offset_map;
	twirl::real_t offset_max;
	twirl::real_t polynomial_err_max;
};

result_t
estimate(const twirl::kannala::full_model &m,
         std::uint16_t width,
         std::uint16_t height);

std::vector<uint32_t>
reformat_offsets(std::vector<twirl::real_t>,
                 twirl::real_t max);

} /* namespace accuracy */
} /* namespace twirr */

#endif	/* TWIRR_ACCURACY_HPP */
