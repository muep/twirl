/*
Copyright (c) 2014 Joonas Sarajärvi

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/
#ifndef TWIRR_CMDLINE_HPP
#define TWIRR_CMDLINE_HPP

#include <exception>
#include <functional>
#include <string>
#include <vector>

#include "compiler_workarounds.hpp"

namespace twirr {
namespace cmdline {

typedef std::string arg_t;
typedef std::vector<arg_t> args_t;
typedef std::string name_t;
typedef std::string prefix_t;

typedef std::function<void(prefix_t, args_t)> action_t;

struct subcmd {
	subcmd(name_t name, action_t action);
	subcmd();

	bool valid() const;

	name_t name;
	action_t action;
};

struct subcmd_name_error : public std::exception {
	subcmd_name_error(std::string usage);
	virtual ~subcmd_name_error();
	virtual const char* what() const noexcept;
	std::string usage;
};

name_t
fixed_name(const char *name);

int
run(const char * const *args, action_t action);

action_t
subcmd_cmd(std::vector<subcmd> subcmds);

} /* namespace cmdline */
} /* namespace twirr */

#endif
